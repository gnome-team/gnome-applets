/*
 * This file is generated by gdbus-codegen, do not modify it.
 *
 * The license of this code is the same as for the D-Bus interface description
 * it was derived from. Note that it links to GLib, so must comply with the
 * LGPL linking clauses.
 */

#ifndef __CPUFREQ_SELECTOR_GEN_H__
#define __CPUFREQ_SELECTOR_GEN_H__

#include <gio/gio.h>

G_BEGIN_DECLS


/* ------------------------------------------------------------------------ */
/* Declarations for org.gnome.CPUFreqSelector */

#define CPUFREQ_TYPE_SELECTOR_GEN (cpufreq_selector_gen_get_type ())
#define CPUFREQ_SELECTOR_GEN(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), CPUFREQ_TYPE_SELECTOR_GEN, CPUFreqSelectorGen))
#define CPUFREQ_IS_SELECTOR_GEN(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), CPUFREQ_TYPE_SELECTOR_GEN))
#define CPUFREQ_SELECTOR_GEN_GET_IFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE ((o), CPUFREQ_TYPE_SELECTOR_GEN, CPUFreqSelectorGenIface))

struct _CPUFreqSelectorGen;
typedef struct _CPUFreqSelectorGen CPUFreqSelectorGen;
typedef struct _CPUFreqSelectorGenIface CPUFreqSelectorGenIface;

struct _CPUFreqSelectorGenIface
{
  GTypeInterface parent_iface;

  gboolean (*handle_can_set) (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation);

  gboolean (*handle_set_frequency) (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation,
    guint arg_cpu,
    guint arg_frequency);

  gboolean (*handle_set_governor) (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation,
    guint arg_cpu,
    const gchar *arg_governor);

};

GType cpufreq_selector_gen_get_type (void) G_GNUC_CONST;

GDBusInterfaceInfo *cpufreq_selector_gen_interface_info (void);
guint cpufreq_selector_gen_override_properties (GObjectClass *klass, guint property_id_begin);


/* D-Bus method call completion functions: */
void cpufreq_selector_gen_complete_can_set (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation,
    gboolean result);

void cpufreq_selector_gen_complete_set_frequency (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation);

void cpufreq_selector_gen_complete_set_governor (
    CPUFreqSelectorGen *object,
    GDBusMethodInvocation *invocation);



/* D-Bus method calls: */
void cpufreq_selector_gen_call_can_set (
    CPUFreqSelectorGen *proxy,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean cpufreq_selector_gen_call_can_set_finish (
    CPUFreqSelectorGen *proxy,
    gboolean *out_result,
    GAsyncResult *res,
    GError **error);

gboolean cpufreq_selector_gen_call_can_set_sync (
    CPUFreqSelectorGen *proxy,
    gboolean *out_result,
    GCancellable *cancellable,
    GError **error);

void cpufreq_selector_gen_call_set_frequency (
    CPUFreqSelectorGen *proxy,
    guint arg_cpu,
    guint arg_frequency,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean cpufreq_selector_gen_call_set_frequency_finish (
    CPUFreqSelectorGen *proxy,
    GAsyncResult *res,
    GError **error);

gboolean cpufreq_selector_gen_call_set_frequency_sync (
    CPUFreqSelectorGen *proxy,
    guint arg_cpu,
    guint arg_frequency,
    GCancellable *cancellable,
    GError **error);

void cpufreq_selector_gen_call_set_governor (
    CPUFreqSelectorGen *proxy,
    guint arg_cpu,
    const gchar *arg_governor,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean cpufreq_selector_gen_call_set_governor_finish (
    CPUFreqSelectorGen *proxy,
    GAsyncResult *res,
    GError **error);

gboolean cpufreq_selector_gen_call_set_governor_sync (
    CPUFreqSelectorGen *proxy,
    guint arg_cpu,
    const gchar *arg_governor,
    GCancellable *cancellable,
    GError **error);



/* ---- */

#define CPUFREQ_TYPE_SELECTOR_GEN_PROXY (cpufreq_selector_gen_proxy_get_type ())
#define CPUFREQ_SELECTOR_GEN_PROXY(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), CPUFREQ_TYPE_SELECTOR_GEN_PROXY, CPUFreqSelectorGenProxy))
#define CPUFREQ_SELECTOR_GEN_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), CPUFREQ_TYPE_SELECTOR_GEN_PROXY, CPUFreqSelectorGenProxyClass))
#define CPUFREQ_SELECTOR_GEN_PROXY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CPUFREQ_TYPE_SELECTOR_GEN_PROXY, CPUFreqSelectorGenProxyClass))
#define CPUFREQ_IS_SELECTOR_GEN_PROXY(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), CPUFREQ_TYPE_SELECTOR_GEN_PROXY))
#define CPUFREQ_IS_SELECTOR_GEN_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), CPUFREQ_TYPE_SELECTOR_GEN_PROXY))

typedef struct _CPUFreqSelectorGenProxy CPUFreqSelectorGenProxy;
typedef struct _CPUFreqSelectorGenProxyClass CPUFreqSelectorGenProxyClass;
typedef struct _CPUFreqSelectorGenProxyPrivate CPUFreqSelectorGenProxyPrivate;

struct _CPUFreqSelectorGenProxy
{
  /*< private >*/
  GDBusProxy parent_instance;
  CPUFreqSelectorGenProxyPrivate *priv;
};

struct _CPUFreqSelectorGenProxyClass
{
  GDBusProxyClass parent_class;
};

GType cpufreq_selector_gen_proxy_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (CPUFreqSelectorGenProxy, g_object_unref)
#endif

void cpufreq_selector_gen_proxy_new (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
CPUFreqSelectorGen *cpufreq_selector_gen_proxy_new_finish (
    GAsyncResult        *res,
    GError             **error);
CPUFreqSelectorGen *cpufreq_selector_gen_proxy_new_sync (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);

void cpufreq_selector_gen_proxy_new_for_bus (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
CPUFreqSelectorGen *cpufreq_selector_gen_proxy_new_for_bus_finish (
    GAsyncResult        *res,
    GError             **error);
CPUFreqSelectorGen *cpufreq_selector_gen_proxy_new_for_bus_sync (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);


/* ---- */

#define CPUFREQ_TYPE_SELECTOR_GEN_SKELETON (cpufreq_selector_gen_skeleton_get_type ())
#define CPUFREQ_SELECTOR_GEN_SKELETON(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), CPUFREQ_TYPE_SELECTOR_GEN_SKELETON, CPUFreqSelectorGenSkeleton))
#define CPUFREQ_SELECTOR_GEN_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), CPUFREQ_TYPE_SELECTOR_GEN_SKELETON, CPUFreqSelectorGenSkeletonClass))
#define CPUFREQ_SELECTOR_GEN_SKELETON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), CPUFREQ_TYPE_SELECTOR_GEN_SKELETON, CPUFreqSelectorGenSkeletonClass))
#define CPUFREQ_IS_SELECTOR_GEN_SKELETON(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), CPUFREQ_TYPE_SELECTOR_GEN_SKELETON))
#define CPUFREQ_IS_SELECTOR_GEN_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), CPUFREQ_TYPE_SELECTOR_GEN_SKELETON))

typedef struct _CPUFreqSelectorGenSkeleton CPUFreqSelectorGenSkeleton;
typedef struct _CPUFreqSelectorGenSkeletonClass CPUFreqSelectorGenSkeletonClass;
typedef struct _CPUFreqSelectorGenSkeletonPrivate CPUFreqSelectorGenSkeletonPrivate;

struct _CPUFreqSelectorGenSkeleton
{
  /*< private >*/
  GDBusInterfaceSkeleton parent_instance;
  CPUFreqSelectorGenSkeletonPrivate *priv;
};

struct _CPUFreqSelectorGenSkeletonClass
{
  GDBusInterfaceSkeletonClass parent_class;
};

GType cpufreq_selector_gen_skeleton_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (CPUFreqSelectorGenSkeleton, g_object_unref)
#endif

CPUFreqSelectorGen *cpufreq_selector_gen_skeleton_new (void);


G_END_DECLS

#endif /* __CPUFREQ_SELECTOR_GEN_H__ */
