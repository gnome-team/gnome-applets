<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN" "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY applet "Keyboard Accessibility Monitor">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Mar 12, 2002
  
-->
<article id="index" lang="cs">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
	 <title>Příručka apletu Stav zpřístupnění klávesnice</title> 
	 <abstract role="description">
	   <para><application>Stav zpřístupnění klávesnice</application> zobrazuje stav funkcí zpřístupnění klávesnice.</para>
	 </abstract>
	 <copyright><year>2005</year> <holder>Francisco Javier F. Serrador</holder></copyright>
	 <copyright><year>2003</year> <holder>Sun Microsystems</holder></copyright> 
<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

	 <publisher role="maintainer"> 
		<publishername>Dokumentační projekt GNOME</publishername> 
	 </publisher> 




	 <authorgroup>
	 	<author><firstname>Francisco Javier F.</firstname> <surname>Serrador</surname></author>
		<author><firstname>Davyd</firstname> <surname>Madeley</surname> <email>davyd@madeley.id.au</email></author>
		<author><firstname>Sun</firstname> <surname>Dokumentační tým GNOME</surname> <affiliation> <orgname>Sun Microsystems</orgname> </affiliation></author> 
      <!-- This is appropriate place for other contributors: translators,
           maintainers,  etc. Commented out by default.
           <othercredit role="translator">
	     <firstname>Latin</firstname> 
	     <surname>Translator 1</surname> 
	     <affiliation> 
	       <orgname>Latin Translation Team</orgname> 
	       <address> <email>translator@gnome.org</email> </address> 
	     </affiliation>
	     <contrib>Latin translation</contrib>
           </othercredit>
-->
	 </authorgroup>
	 
	 <releaseinfo revision="2.26" role="review"/>

	 <revhistory>
	        <revision><revnumber>Verze 2.12</revnumber> <date>srpen 2005</date> <revdescription> 
			 <para role="author">Dokumentační tým GNOME</para> 
			 <para role="publisher">Dokumentační projekt GNOME</para> 
		  </revdescription></revision> 	   
		<revision><revnumber>Příručka V0.1 k apletu Stav zpřístupnění klávesnice</revnumber> <date>březen 2003</date> <revdescription> 
			 <para role="author">Dokumentační tým GNOME společnosti Sun</para> 
			 <para role="publisher">Dokumentační projekt GNOME</para> 
		  </revdescription></revision> 
		<revision><revnumber>Příručka V0.1 k apletu Stav zpřístupnění klávesnice</revnumber> <date>březen 2003</date> <revdescription> 
			 <para role="author">Bill Haneman</para> 
			 <para role="publisher">Dokumentační projekt GNOME</para> 
		  </revdescription></revision> 
	 </revhistory> 
	 <releaseinfo>Tato příručka popisuje Stav zpřístupnění klávesnice ve verzi 2.12</releaseinfo> 
	 <legalnotice> 
		<title>Ohlasy</title> 
		<para>Pokud chcete oznámit chybu nebo navrhnout vylepšení vztahující se k appletu Stav zpřístupnění klávesnice nebo této příručce, postupujte dle instrukcí na stránce <ulink url="ghelp:gnome-feedback" type="help">Stránka s ohlasy na GNOME</ulink>.</para>
		
	 </legalnotice> 
  
    <othercredit class="translator">
      <personname>
        <firstname>Lucas Lommer</firstname>
      </personname>
      <email>llommer@svn.gnome.org</email>
    </othercredit>
    <copyright>
      
        <year>2009.</year>
      
      <holder>Lucas Lommer</holder>
    </copyright>
  </articleinfo> 
  <indexterm><primary>Stav zpřístupnění klávesnice</primary></indexterm> 

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<sect1 id="accessx-applet-introduction">
<title>Použití</title>
<!-- ==== Figure ============================================= -->
	 <figure id="accessx-applet-fig"> 
		<title>Stav zpřístupnění klávesnice</title> 
		<screenshot> 
		  <mediaobject><imageobject><imagedata fileref="figures/accessx-status-applet.png" format="PNG"/></imageobject> <textobject><phrase>Ukazuje Stav zpřístupnění klávesnice. Obsahuje řádek ikon představujících stav klávesnice.</phrase></textobject></mediaobject> 
		</screenshot> 
	 </figure> 
    <!-- ==== End of Figure ======================================= -->
<para><application>Stav zpřístupnění klávesnice</application> zobrazuje stav funkcí zpřístupnění klávesnice, pokud jsou používány. Například uvidíte, který modifikátor je právě aktivní a která byla stisknuta tlačítka myši pomocí klávesnice.</para>
<para>Více informací o funkcích zpřístupnění klávesnice popisuje dokument <ulink url="ghelp:gnome-access-guide">Příručka k zpřístupnění</ulink>.</para>

<sect2 id="accessx-status-interpreting">
<title>Co aplet zobrazuje</title>
<para>
  <informaltable>
    <tgroup cols="2">
      <colspec colname="col1" colwidth="0.81"/><colspec colname="col2" colwidth="1.19*"/>
      <thead>
	<row valign="top">
	  <entry colname="col1">
	    <para>Symbol</para>
	  </entry>
	  <entry colname="col">
	    <para>Význam</para>
	  </entry>
	</row>
      </thead>
      <tbody>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx-status-disabled.png" format="PNG"/></imageobject> <textobject><phrase>Stav při vypnutí zpřístupnění</phrase> </textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Funkce zpřístupnění jsou momentálně vypnuté.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_bounce-keys.png" format="PNG"/></imageobject> <textobject><phrase>Vícenásobná zmáčknutí kláves</phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Funkce Vícenásobná stisknutí kláves je aktivována.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_mouse-keys.png" format="PNG"/></imageobject> <textobject><phrase>Myš klávesnicí</phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Funkce Myš klávesnicí je aktivována.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject> <imagedata fileref="figures/accessx_slow-keys.png" format="PNG"/></imageobject> <textobject><phrase>Pomalé klávesy</phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Funkce Pomalé klávesy je aktivována.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_sticky-keys.png" format="PNG"/></imageobject> <textobject><phrase>Kombinace kláves jedním prstem</phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Funkce Kombinace kláves jedním prstem je aktivována. Zde zobrazuje jedno zmáčknutí klávesy <keycap>Shift</keycap> a dvě zmáčknutí klávesy <keycap>Ctrl</keycap>.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject> <imagedata fileref="figures/accessx_sticky-altGr-key.png" format="PNG"/></imageobject> <textobject><phrase>Symbol klávesy <keycap>AltGr</keycap></phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Klávesa <keycap>AltGr</keycap> byla jednou zmáčknuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_sticky-meta-key.png" format="PNG"/></imageobject> <textobject><phrase>Symbol klávesy <keycap>Meta</keycap></phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Klávesa <keycap>Meta</keycap> byla jednou zmáčknuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_sticky-windows-key.png" format="PNG"/></imageobject> <textobject><phrase>Symbol klávesy <keycap>Windows</keycap></phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Klávesa <keycap>Windows</keycap> byla jednou zmáčknuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para><inlinemediaobject><imageobject><imagedata fileref="figures/accessx_sticky-shift-key.png" format="PNG"/></imageobject> <textobject><phrase>Symbol klávesy Shift</phrase></textobject></inlinemediaobject></para>
	  </entry>
	  <entry colname="col2">
	    <para>Klávesa <keycap>Shift</keycap> byla jednou zmáčknuta.</para>
	  </entry>
	</row>
      </tbody>
    </tgroup>
  </informaltable>
 </para>
</sect2>
</sect1>

</article>
