<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN" "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY applet "Keyboard Accessibility Monitor">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Mar 12, 2002
  
-->
<article id="index" lang="eu">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
	 <title>Teklatuko erabilerraztasun-monitorearen eskuliburua</title> 
	 <abstract role="description">
	   <para><application>Teklatuko erabilerraztasun-monitoreak</application> teklatuaren erabilerraztasunaren eginbideen egoera erakusten du.</para>
	 </abstract>
	 <copyright lang="en">
	 	<year>2005</year>
		<holder>Francisco Javier F. Serrador</holder>
	 </copyright>
	 <copyright lang="en">
		<year>2003</year> 
		<holder>Sun Microsystems</holder> 
	 </copyright> 
<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

	 <publisher role="maintainer"> 
		<publishername>GNOMEren dokumentazio-proiektua</publishername> 
	 </publisher> 




	 <authorgroup>
	 	<author lang="en">
		  <firstname>Francisco Javier F.</firstname>
		  <surname>Serrador</surname>
		</author>
		<author lang="en">
		  <firstname>Davyd</firstname>
		  <surname>Madeley</surname>
		  <email>davyd@madeley.id.au</email>
		</author>
		<author lang="en"> 
		  <firstname>Sun</firstname> 
		  <surname>GNOME Documentation Team</surname> 
		  <affiliation> 
			 <orgname>Sun Microsystems</orgname> 
		  </affiliation> 
		</author> 
      <!-- This is appropriate place for other contributors: translators,
           maintainers,  etc. Commented out by default.
           <othercredit role="translator">
	     <firstname>Latin</firstname> 
	     <surname>Translator 1</surname> 
	     <affiliation> 
	       <orgname>Latin Translation Team</orgname> 
	       <address> <email>translator@gnome.org</email> </address> 
	     </affiliation>
	     <contrib>Latin translation</contrib>
           </othercredit>
-->
	 </authorgroup>
	 
	 <releaseinfo revision="2.26" role="review"/>

	 <revhistory>
	        <revision lang="en"> 
		  <revnumber>v. 2.12</revnumber> 
		  <date>August 2005</date> 
		  <revdescription> 
			 <para role="author" lang="en">GNOME Documentation Team
				</para> 
			 <para role="publisher" lang="en">GNOME Documentation Project</para> 
		  </revdescription> 
		</revision> 	   
		<revision lang="en"> 
		  <revnumber>Keyboard Accessibility Monitor Applet Manual V0.1</revnumber> 
		  <date>March 2003</date> 
		  <revdescription> 
			 <para role="author" lang="en">Sun GNOME Documentation Team
				</para> 
			 <para role="publisher" lang="en">GNOME Documentation Project</para> 
		  </revdescription> 
		</revision> 
		<revision lang="en"> 
		  <revnumber>Keyboard Accessibility Status Applet Manual V0.1</revnumber> 
		  <date>May 2003</date> 
		  <revdescription> 
			 <para role="author" lang="en">Bill Haneman
				</para> 
			 <para role="publisher" lang="en">GNOME Documentation Project</para> 
		  </revdescription> 
		</revision> 
	 </revhistory> 
	 <releaseinfo>Eskuliburu honetan Teklatuko erabilerraztasun-monitorearen 2.12 bertsioa azaltzen da.</releaseinfo> 
	 <legalnotice> 
		<title>Ohar-bidaltzea</title> 
		<para>Teklatuko erabilerraztasun-monitorearen miniaplikazioari buruzko akatsen berri emateko edo oharrak bidaltzeko, <ulink url="ghelp:gnome-feedback" type="help">GNOMEra oharrak bidaltzeko orrian</ulink> aurkituko dituzu argibideak.</para>
		
	 </legalnotice> 
  </articleinfo> 
  <indexterm lang="en"> 
	 <primary>Keyboard Accessibility Status</primary> 
  </indexterm> 

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<sect1 id="accessx-applet-introduction">
<title>Erabilera</title>
<!-- ==== Figure ============================================= -->
	 <figure id="accessx-applet-fig"> 
		<title>Teklatuko erabilerraztasun-monitorea</title> 
		<screenshot> 
		  <mediaobject lang="en"> 
			 <imageobject><imagedata fileref="figures/accessx-status-applet.png" format="PNG"/> 
			 </imageobject>
			 <textobject> 
				<phrase>Shows Keyboard Accessibility Monitor. Contains row of icons representing keyboard state.</phrase> 
			 </textobject> 
		  </mediaobject> 
		</screenshot> 
	 </figure> 
    <!-- ==== End of Figure ======================================= -->
<para><application>Teklatuko erabilerraztasun-monitoreak</application> teklatuaren erabilerraztasun-eginbideen egoera erakusten dizu, erabiltzen ari bazara. Esate baterako, ikus dezakezu zein tekla aldatzaile dauden aktibo, eta saguaren zein botoi sakatzen diren teklatu bidez.</para>
<para>Teklatuaren erabilerraztasun-eginbideak erabiltzeari buruz gehiago jakiteko, ikus <ulink url="ghelp:gnome-access-guide">Mahaigaineko Erabilerraztasunaren Gida</ulink>.</para>

<sect2 id="accessx-status-interpreting">
<title>Miniaplikazioak erakusten duena</title>
<para>
  <informaltable>
    <tgroup cols="2">
      <colspec colname="col1" colwidth="0.81"/><colspec colname="col2" colwidth="1.19*"/>
      <thead>
	<row valign="top">
	  <entry colname="col1">
	    <para>Ikurra</para>
	  </entry>
	  <entry colname="col">
	    <para>Esanahia</para>
	  </entry>
	</row>
      </thead>
      <tbody>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx-status-disabled.png" format="PNG"/>
	      </imageobject><textobject><phrase>Status disabled</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para>Erabilerraztasunaren eginbideak desgaituta daude une honetan.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_bounce-keys.png" format="PNG"/>
	      </imageobject><textobject><phrase>Bounce Keys</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para>Errebote-teklak aktibatuta daude.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_mouse-keys.png" format="PNG"/>
	      </imageobject><textobject><phrase>Mouse Keys</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para>Sagu-teklak aktibatuta daude.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_slow-keys.png" format="PNG"/>
	      </imageobject><textobject><phrase>Slow Keys</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para>Tekla motelak aktibatuta daude.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_sticky-keys.png" format="PNG"/>
	      </imageobject><textobject><phrase>Sticky Keys</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para>Tekla itsaskorrak aktibatuta daude. Hemen azaltzen da <keycap>Maius</keycap> tekla behin sakatuta eta <keycap>Ktrl</keycap> tekla bitan sakatuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_sticky-altGr-key.png" format="PNG"/>
	      </imageobject><textobject><phrase><keycap>AltGr</keycap> Key Symbol</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para><keycap>AltGr</keycap> tekla behin sakatuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_sticky-meta-key.png" format="PNG"/>
	      </imageobject><textobject><phrase><keycap>Meta</keycap> Key Symbol</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para><keycap>Meta</keycap> tekla behin sakatuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_sticky-windows-key.png" format="PNG"/>
	      </imageobject><textobject><phrase><keycap>Windows</keycap> logo key</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para><keycap>Windows</keycap> logoaren tekla behin sakatuta.</para>
	  </entry>
	</row>
	<row valign="top">
	  <entry colname="col1">
	    <para lang="en">
	      <inlinemediaobject><imageobject>
		<imagedata fileref="figures/accessx_sticky-shift-key.png" format="PNG"/>
	      </imageobject><textobject><phrase>Shift Symbol</phrase>
	    </textobject></inlinemediaobject>
	    </para>
	  </entry>
	  <entry colname="col2">
	    <para><keycap>Maius</keycap> tekla behin sakatuta.</para>
	  </entry>
	</row>
      </tbody>
    </tgroup>
  </informaltable>
 </para>
</sect2>
</sect1>

</article>
