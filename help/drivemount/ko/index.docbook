<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "2.10">
<!ENTITY manrevision "2.10">
<!ENTITY date "March 2005">
<!ENTITY app "<application>Disk Mounter</application>">
<!ENTITY appname "Disk Mounter">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="ko">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
    <title>드라이브 마운트 설명서</title>
    <abstract role="description">
      <para>디스크 마운트를 이용해서 여러가지 종류의 드라이브와 파일 시스템을 패널에서 마운트하고 마운트 해제할 수 있습니다.</para>
    </abstract>
    <copyright lang="en">
      <year>2005</year>
      <holder>Trent Lloyd</holder>
    </copyright>
    <copyright lang="en">
      <year>2004</year>
      <holder>Sun Microsystems</holder>
    </copyright>
    <copyright lang="en"> 
      <year>2002</year> 
      <holder>John Fleck</holder> 
    </copyright> 
    <copyright lang="en"> 
      <year>2000</year> 
      <holder>Dan Mueth</holder> 
    </copyright>
<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
    <publisher role="maintainer"> 
      <publishername>그놈 문서 프로젝트</publishername> 
    </publisher> 

   
   <!-- This file  contains link to license for the documentation (GNU FDL), and 
        other legal stuff such as "NO WARRANTY" statement. Please do not change 
	any of this. -->

    <authorgroup> 
      <author lang="en">
        <firstname>Trent</firstname>
        <surname>Lloyd</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address><email>lathiat@bur.st</email></address>
        </affiliation>
      </author>
        
      <author lang="en">
        <firstname>Sun</firstname>
        <surname>GNOME Documentation Team</surname>
        <affiliation><orgname>Sun Microsystems</orgname></affiliation>
      </author>
      <author lang="en"> 
	<firstname>John </firstname> 
	<surname> Fleck</surname> 
	<affiliation> 
	  <orgname>GNOME Documentation Project</orgname> 
	  <address> <email>jfleck@inkstain.net</email> </address> 
	</affiliation> 
      </author> 
      <author lang="en"> 
	<firstname>Dan </firstname> 
	<surname> Mueth</surname> 
	<affiliation> 
	  <orgname>GNOME Documentation Project</orgname> 
	  <address> <email>muet@alumni.uchicago.edu</email> </address> 
	</affiliation> 
      </author> 
<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>
	
	<releaseinfo revision="2.26" role="review"/>


    <revhistory>
      <revision lang="en"> 
	<revnumber>Version 2.10</revnumber> 
	<date>March 2005</date> 
	<revdescription> 
          <para role="author" lang="en">Trent Lloyd</para>
          <para role="publisher" lang="en">GNOME Documentation Project</para>
	</revdescription> 
      </revision> 
      <revision lang="en"> 
	<revnumber>Disk Mounter Applet Manual V2.1</revnumber> 
	<date>February 2004</date> 
	<revdescription> 
          <para role="author" lang="en">Sun GNOME Documentation Team</para>
          <para role="publisher" lang="en">GNOME Documentation Project</para>
	</revdescription> 
      </revision> 
      <revision lang="en"> 
	<revnumber>Disk Mounter Applet Manual V2.0</revnumber> 
	<date>March 2002</date> 
	<revdescription> 
	  <para role="author" lang="en">John Fleck
	    <email>jfleck@inkstain.net</email>
	  </para>
	  <para role="publisher" lang="en">GNOME Documentation Project</para>
	</revdescription> 
      </revision> 
      <revision lang="en"> 
	<revnumber>Drive Mount Applet Manual</revnumber> 
	<date>April 2000</date> 
	<revdescription> 
	  <para role="author" lang="en">Dan Mueth 
	    <email>muet@alumni.uchicago.edu</email>
	  </para>
	  <para role="publisher" lang="en">GNOME Documentation Project</para>
	</revdescription> 
      </revision> 
    </revhistory> 

    <releaseinfo>이 설명서는 디스크 마운트 버전 2.10에 대해 설명합니다.</releaseinfo>
    <legalnotice> 
      <title>피드백</title> 
      <para><application>디스크 마운트</application> 또는 이 설명서에 대한 버그를 보고하거나 의견을 제시하려면 <ulink url="ghelp:gnome-feedback" type="help">그놈 피드백 페이지</ulink>에 있는 대로 하십시오.</para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice> 
  </articleinfo> 

  <indexterm zone="index" lang="en"> 
    <primary>Disk Mounter</primary> 
  </indexterm> 
  

  <!-- ============= Introduction  ================================ -->
  <sect1 id="drivemountapplet-intro">
    <title>소개</title>

    <figure id="drivemountapplet-fig"> 
      <title>디스크 마운트</title> 
      <screenshot> 
        <mediaobject lang="en"> 
          <imageobject><imagedata fileref="figures/drivemount-applet_example.png" format="PNG"/> 
          </imageobject>
          <textobject> 
            <phrase>The Disk Mounter.</phrase> 
          </textobject> 
        </mediaobject> 
      </screenshot> 
    </figure>

    <para><application>디스크 마운트</application>를 이용해서 여러가지 종류의 드라이브와 파일 시스템을 마운트하고 마운트 해제할 수 있습니다.</para>
    <para><application>디스크 마운트</application>가 제대로 작동하려면 시스템 관리자가 시스템을 적절하게 설정해야 합니다. 어떤 시스템 관리 작업이 필요한 지에 대한 자세한 내용은 <ulink url="man:fstab" type="man"><citerefentry><refentrytitle>fstab</refentrytitle><manvolnum>5</manvolnum></citerefentry></ulink> 부분을 참고하십시오.</para>

    <sect2 id="drivemount-intro-add">
     <title>디스크 마운트를 패널에 추가하려면</title>
     <para><application>디스크 마운트</application>를 패널에 추가하려면, 패널에 마우스 오른쪽 단추를 눌러 <guimenuitem>패널에 추가</guimenuitem>를 선택하십시오. <application>패널에 추가</application> 대화 상자에서 <application>디스크 마운트</application>를 선택하고, <guibutton>확인</guibutton>을 누르십시오.</para>
    </sect2>
 
    <sect2 id="drivemount-intro-mount">
      <title>수동으로 파일 시스템 마운트하기 및 마운트 해제하기</title>
      <para>리눅스 및 유닉스 시스템의 파일 시스템 상당수는 수동으로 마운트하고 마운트 해제해야 합니다.</para>
      <para>어떤 파일 시스템을 마운트하면, 그 파일 시스템에서 파일을 읽고 쓸 수 있습니다. 파일 시스템 작업이 모두 끝나면 그 파일 시스템을 마운트 해제해야 합니다.</para>
      <para>플로피 디스크와 ZIP 디스크와 같은 이동식 드라이브는 미디어를 빼기 전에 그 마운트를 해제해야 합니다. 리눅스 및 유닉스 시스템에서는 변경 사항을 즉시 미디어에 쓰지 않기 때문입니다. 이러한 시스템에서는 시스템의 속도를 높이기 위해 디스크의 변경 사항을 버퍼링합니다.</para>
      <para>하드 드라이브같은 고정된 드라이브의 파티션은 일반적으로는 컴퓨터가 부팅할 때 자동으로 마운트하고 컴퓨터를 끌 때 자동으로 마운트 해제합니다. 이동식 미디어는 수동으로 마운트하고 마운트 해제합니다. 한 가지 방법으로 <application>디스크 마운트</application>를 이용합니다.</para>
      <para>시스템에 따라서 자동으로 특정 방식의 (USB나 IEEE1394 디스크와 같은) 이동식 미디어를 마운트하기도 합니다. 이러한 장치도 <application>디스크 마운트</application> 안에 나타나므로, 사용을 마쳤을 때 그 장치의 마운트를 해제할 수 있고, 그 장치가 연결되어 있다는 사실을 알려줍니다.</para>
    </sect2>

  </sect1>

  <!-- ============= Usage  ================================ -->
  <sect1 id="drivemount-usage">
    <title>사용법</title>

    <sect2 id="drivemount-usage-tooltip">
    <title>드라이브의 이름과 마운트 상태를 표시하려면</title>
      <para>드라이브의 이름과 마운트 상태를 표시하려면 마우스 포인터를 패널에 있는 드라이브 아이콘으로 위에 옮깁니다. 도구 설명에 드라이브의 이름과 마운트 상태를 표시합니다.</para>
      <screenshot><mediaobject><imageobject>
       <imagedata fileref="figures/drivemount-applet_status.png" format="PNG"/>
      </imageobject></mediaobject></screenshot>
    </sect2>

    <sect2 id="drivemount-usage-mount">
    <title>드라이브를 마운트하거나, 마운트 해제하거나, 꺼내려면</title>
      <para>드라이브를 마운트하려면, 패널에 있는 드라이브 아이콘을 누르고 <guimenuitem>드라이브 마운트</guimenuitem>을 선택하십시오.</para>
      <screenshot><mediaobject><imageobject>
       <imagedata fileref="figures/drivemount-applet_mount.png" format="PNG"/>
      </imageobject></mediaobject></screenshot>
      <para>드라이브의 마운트를 해제하려면, 패널의 드라이브 아이콘을 누르고 <guimenuitem>드라이브 마운트 해제</guimenuitem> 옵션을 선택하십시오. 그 장치가 CD-ROM 드라이브인 경우, 그 메뉴 항목이 <guimenuitem>드라이브 꺼내기</guimenuitem>일 수도 있습니다.</para>
      <screenshot><mediaobject><imageobject>
       <imagedata fileref="figures/drivemount-applet_eject.png" format="PNG"/>
      </imageobject></mediaobject></screenshot>
    </sect2>

    <sect2 id="drivemount-usage-browse">
    <title>드라이브의 내용을 살펴보려면</title>
      <para>파일 관리자로 드라이브의 내용을 살펴보려면, 패널의 드라이브 아이콘을 누르고, <guimenuitem>드라이브 열기</guimenuitem>를 선택하십시오.</para>
      <screenshot><mediaobject><imageobject>
       <imagedata fileref="figures/drivemount-applet_open.png" format="PNG"/>
      </imageobject></mediaobject></screenshot>
      <para>마운트한 드라이브의 내용만 살펴볼 수 있습니다.</para>
    </sect2>
  </sect1>
</article>
