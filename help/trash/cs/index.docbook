<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.14">
<!ENTITY manrevision "2.11">
<!ENTITY date "August 2006">
<!ENTITY applet "Panel Trash">
]>
<article id="index" lang="cs">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
	 <title>Příručka appletu Koš</title> 
	 <abstract role="description">
	   <para>Applet Koš vám dovolí spravovat váš koš z panelu.</para>
	 </abstract>
	 <copyright><year>2006</year> <holder>Dokumentační projekt GNOME</holder></copyright>	 
	 <copyright><year>2005</year> <holder>Davyd Madeley</holder></copyright>
	 <copyright><year>2004</year> <holder>Michiel Sikkes</holder></copyright> 
<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
<!-- An address can be added to the publisher information.  If a role is 
     not specified, the publisher/author is the same for all versions of the 
     document.  -->
	 <publisher role="maintainer"> 
		<publishername>Dokumentační projekt GNOME</publishername> 
	 </publisher> 


<!-- This file  contains link to license for the documentation (GNU FDL), and 
     other legal stuff such as "NO WARRANTY" statement. Please do not change 
     any of this. -->

	 <authorgroup> 
		<author><firstname>Michiel</firstname> <surname>Sikkes</surname> <affiliation> <orgname>GNOME Documentation Project</orgname> <address> <email>michiel@eyesopened.nl</email> </address> </affiliation></author> 
		<author><firstname>Davyd</firstname><surname>Madeley</surname> <affiliation> <orgname>GNOME Project</orgname> <address><email>davyd@madeley.id.au</email></address> </affiliation></author>
      <!-- This is appropriate place for other contributors: translators,
           maintainers,  etc. Commented out by default.
           <othercredit role="translator">
	     <firstname>Michiel</firstname> 
	     <surname>Sikkes</surname> 
	     <affiliation> 
	       <orgname>Latin Translation Team</orgname> 
	       <address> <email>translator@gnome.org</email> </address> 
	     </affiliation>
	     <contrib>Latin translation</contrib>
           </othercredit>
-->
	 </authorgroup>
	 
	 <releaseinfo revision="2.26" role="review"/>
	 
	 <revhistory>
	   <revision><revnumber>Verze 2.10</revnumber> <date>březen 2005</date> <revdescription>
	       <para role="author">Davyd Madeley</para>
         <para role="publisher">Dokumentační projekt GNOME</para> 	       
	     </revdescription></revision>
		<revision><revnumber>Příružka V2.11 k apletu verze 2.8</revnumber> <date>červenec 2004</date> <revdescription> 
			 <para role="author">Michiel Sikkes</para> 
			 <para role="publisher">Dokumentační projekt GNOME</para> 
		  </revdescription></revision>
	  </revhistory>
	 <releaseinfo>Tato příručka popisuje applet Koš ve verzi 2.14.</releaseinfo>
	 <legalnotice> 
		<title>Odezva</title> 
		<para>Pokud chcete oznámit chybu nebo navrhnout vylepšení tohoto appletu nebo této příručky, můžete přidat hlášení o chybě do příslušné sekce <ulink url="ghelp:user-guide?feedback" type="help">Stránka s ohlasy na GNOME</ulink>.</para>
		
	 </legalnotice> 
  
    <othercredit class="translator">
      <personname>
        <firstname>Jan Brož</firstname>
      </personname>
      <email>brozj@email.cz</email>
    </othercredit>
    <copyright>
      
        <year>2009.</year>
      
      <holder>Jan Brož</holder>
    </copyright>
  </articleinfo>
  <indexterm><primary>Koš</primary></indexterm>

  <sect1 id="trash-introduction"> 
	 <title>Úvod</title> 
<para>Applet <application>Koš</application> vám dovolí spravovat váš koš z panelu.</para>
<para>Koš ve vašem panelu se chová stejně jako Koš na vaší pracovní ploše, užitečné je, že panely jsou vždy viditelné.</para>

<sect2 id="trash-introduction-add">
<title>Přidání appletu Koš na panel</title> 
  <para>Pokud chcete přidat <application>applet Koš</application> na panel, proveďte následující kroky:</para>
    <orderedlist>
      <listitem>
        <para>Klikněte pravým tlačítkem na místo na panel, kam chcete applet přidat.</para>
      </listitem>
      <listitem>
        <para>Zvolte <guimenuitem>Přidat na panel</guimenuitem>.</para>
      </listitem>
      <listitem>
        <para>Přesuňte se níže v položkách okna <guilabel>Přidat na panel</guilabel> a zvolte <guilabel>Koš</guilabel>.</para>
      </listitem>
      <listitem>
        <para>Klikněte na <guibutton>Přidat</guibutton>.</para>
      </listitem>
    </orderedlist>
  <para>Více informací o práci a panely popisuje <ulink type="help" url="ghelp:user-guide?panels">Příručka uživatele pracovní plochy</ulink>.</para>

</sect2>
</sect1>

<sect1 id="trash-usage">
	<title>Použití</title>
         <figure id="cpufreq-applet-figure">
            <title>Koš v panelu</title>
                <screenshot>
                  <mediaobject>
                     <imageobject>
                        <imagedata fileref="figures/trash-applet.png" format="PNG"/>
                     </imageobject>
                  </mediaobject>
               </screenshot>
         </figure>

	<sect2 id="trash-usage-moving">
	<title>Přesunutí položek do koše</title>
	<para>Položky do koše přesunete jejich označením ve správci souborů a přetažením na applet <application>Koš</application>.</para>
	</sect2>

	<sect2 id="trash-usage-emptying">
	<title>Vysypání koš</title>
	<para>Pokud chcete trvale odstranit všechny položky v koši, klikněte pravým tlačítkem na applet a zvolte <menuchoice><guimenuitem>Vyprázdnit koš</guimenuitem></menuchoice>.</para>
	</sect2>

	<sect2 id="trash-usage-opening">
	<title>Zobrazení obsahu koše</title>
	<para>Pokud chcete zobrazit obsah koše, klikněte na applet pravým tlačítkem a zvolte <menuchoice><guimenuitem>Otevřít</guimenuitem></menuchoice>. Okno koše se otevře ve správci souborů <application>Nautilus</application>.</para>
	</sect2>
</sect1>
</article>
