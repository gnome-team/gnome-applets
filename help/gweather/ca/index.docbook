<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.12">
<!ENTITY applet "Weather Report">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="ca">
   <!-- please do not change the id; for translations, change lang to -->
   <!-- appropriate code -->
     
   <articleinfo>
      <title>Manual del butlletí meteorològic</title>
      <abstract role="description">
	<para>La miniaplicació del butlletí meteorològic baixa la informació del temps d'Internet per a una ubicació determinada i mostra la temperatura i un símbol que representa el temps actual en el quadre. En fer-hi clic s'obté més informació, com ara com la previsió, el temps de sortida i de posta del sol, la direcció del vent, la pressió i més. Totes les unitats són personalitzables.</para>
      </abstract>
      <copyright lang="en">
        <year>2005</year>
	<holder>Davyd Madeley</holder>
      </copyright>
      <copyright lang="en">
	<year>2004</year>
	<holder>Angela Boyle</holder>
      </copyright>
      <copyright lang="en">
		<year>2002</year>
		<year>2003</year>
		<year>2004</year>
		<holder>Sun Microsystems</holder>    	 
      </copyright>
      <copyright lang="en">
		<year>1999</year>  		
		<year>2000</year>
		<holder>Spiros Papadimitriou</holder>  	 
      </copyright>
      <copyright lang="en">
		<year>1999</year>  		
		<year>2000</year>
		<holder>Dan Mueth</holder>  	 
      </copyright>
      <!-- translators: uncomment this:
      
        <copyright>
         <year>2002</year>
         <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
        </copyright>
      
         -->
      <!-- An address can be added to the publisher information.  If a role is 
           not specified, the publisher/author is the same for all versions of the 
           document.  -->
      	 
      <publisher role="maintainer">
		<publishername>Projecte de documentació del GNOME</publishername>  	 
      </publisher>
        
      <!-- This file  contains link to license for the documentation (GNU FDL), and 
           other legal stuff such as "NO WARRANTY" statement. Please do not change 
           any of this. -->
      	 
      <authorgroup>
        <author lang="en">
	  <firstname>Davyd</firstname><surname>Madeley</surname>
	  <affiliation>
	   <orgname>GNOME Project</orgname>
	   <address><email>davyd@madeley.id.au</email></address>
	  </affiliation>
	</author>
	<author lang="en">
	  <firstname>Angela</firstname>
	  <surname>Boyle</surname>
	</author>
           		
	<author lang="en">
	<firstname>Sun </firstname>  		  
	<surname>GNOME Documentation Team</surname>            		  

            <affiliation>
		<orgname>Sun Microsystems</orgname>  			 
            </affiliation>
              		
        </author>
           		
        <author lang="en">
	<firstname>Spiros</firstname>  		  
	<surname>Papadimitriou</surname>
             		  
            <affiliation>
		<orgname>GNOME Documentation Project</orgname>  			 
		<address><email>spapadim+@cs.cmu.edu</email> </address>  		  
            </affiliation>
              		
         </author>
           		
         <author lang="en">
	 <firstname>Dan</firstname>
	 <surname>Mueth</surname>  	
	  
	     <affiliation>
 		<orgname>GNOME Documentation Project</orgname>  			 
		<address><email>d-mueth@uchicago.edu</email> </address>  		  
             </affiliation>
              		
         </author>
               
         <!-- This is appropriate place for other contributors: translators,
                    maintainers,  etc. Commented out by default.
                    <othercredit role="translator">
         	     <firstname>Latin</firstname> 
         	     <surname>Translator 1</surname> 
         	     <affiliation> 
         	       <orgname>Latin Translation Team</orgname> 
         	       <address> <email>translator@gnome.org</email> </address> 
         	     </affiliation>
         	     <contrib>Latin translation</contrib>
                    </othercredit>
         -->
         	 
      </authorgroup>
	  
	  <releaseinfo revision="2.26" role="review"/>
      	 
      <revhistory>
        <revision lang="en">
	  <revnumber>Version 2.12</revnumber>
	  <date>March 2005</date>
	  <revdescription>
	    <para role="author" lang="en">Davyd Madeley</para>
	  </revdescription>
	</revision>
        <revision lang="en">
	  <revnumber>Version 2.10</revnumber>
	  <date>March 2005</date>
	  <revdescription>
	    <para role="author" lang="en">Davyd Madeley</para>
	  </revdescription>
	</revision>
	<revision lang="en">
	  <revnumber>Version 2.8</revnumber>
	  <date>September 2004</date>
	  <revdescription>
	    <para role="author" lang="en">Angela Boyle</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
	  </revdescription>
	</revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.4</revnumber>  		
          <date>February 2004</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.3</revnumber>  		
          <date>January 2003</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>  
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.2</revnumber>  		
          <date>August 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>  
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.1</revnumber>  		
          <date>July 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.0</revnumber>  		
          <date>March 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
	  <revnumber>GNOME Weather Applet</revnumber>  		  
	  <date>2000</date>
          <revdescription>
            <para role="author" lang="en">
              Spiros Papadimitriou 				
              <email>spapadim+@cs.cmu.edu</email>
            </para>
            <para role="author" lang="en">
              Dan Mueth 				
	      <email>d-mueth@uchicago.edu</email>
            </para>
            <para role="publisher" lang="en">
              GNOME Documentation Project
            </para>
          </revdescription>
        </revision>
      </revhistory>
        	 
      <releaseinfo>Aquest manual descriu la versió 2.12 del butlletí meteorològic.</releaseinfo>
        	 
      <legalnotice>
         <title>Comentaris</title>  		
         <para>Per a informar d'un error o fer un suggeriment sobre la miniaplicació del butlletí meteorològic o d'aquest manual, seguiu les indicacions de la <ulink url="ghelp:gnome-feedback" type="help">pàgina de suggeriments del GNOME</ulink>.</para>
         		 	 
      </legalnotice>
          
   </articleinfo>
       <indexterm lang="en">  	 
       <primary>Weather Report</primary>    
       </indexterm> 
   <!-- ============= Document Body ============================= -->
   <!-- ============= Introduction ============================== -->
   <sect1 id="gweather-introduction">
      <title>Introducció</title>
      <!-- ==== Figure ============================================= -->
      	 
      <figure id="weather-applet-fig">
	<title>Informe del butlletí meteorològic</title>  		
         <screenshot>             		  
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather_applet.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Shows Weather Report. Contains a weather icon and current temperature</phrase> 			 
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
       
      <!-- ==== End of Figure ======================================= -->
      <para>El <application>butlletí meteorològic</application> baixa la informació del temps dels servidors de l'U.S. National Weather Service (NWS), incloent la Interactive Weather Information Network (IWIN) i altres serveis meteorològics. Podeu utilitzar el <application>butlletí meteorològic</application> per a mostrar la informació del temps actual i per a tenir un pronòstic al vostre ordinador.</para>
      <note id="proxies-note-1">
         <para>Si l'equip es troba darrere d'un tallafoc, haureu d'utilitzar un servidor intermediari per a accedir als servidors del temps. Si heu de configurar l'escriptori del GNOME per a utilitzar un servidor intermediari, utilitzeu les eines de preferència per a especificar el servidor intermediari de xarxa per a connexions a Internet.</para>
      </note>   
      <para>El <application>butlletí meteorològic</application> mostra la informació següent per a la ubicació per defecte o una ubicació que especifiqueu:</para>
      <itemizedlist>
         <listitem>
            <para>Una icona del temps que representa les condicions meteorològiques generals. Vegeu la <xref linkend="gweather-introduction-icons"/>.</para>
         </listitem>
         <listitem>
            <para>La temperatura actual.</para>
         </listitem>
      </itemizedlist>

    <sect2 id="gweather-introduction-icons">
     <title>Icones del temps al quadre</title>
     <informaltable frame="all">
      <tgroup cols="2" colsep="1" rowsep="1">
       <colspec colname="COLSPEC0" colwidth="50*"/>
       <colspec colname="COLSPEC1" colwidth="50*"/>
       <thead>
        <row valign="top">
	 <entry colname="COLSPEC0"><para>Icona</para></entry>
	 <entry colname="COLSPEC1"><para>Descripció</para></entry>
	</row>
       </thead>
       <tbody>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-sunny.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Sunny</phrase>
	      </textobject></inlinemediaobject>
	      <inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-night-clear.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Night</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Està serè amb bon temps.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-few-clouds.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Few Clouds</phrase>
	      </textobject></inlinemediaobject>
	      <inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-night-few-clouds.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Night Few Clouds</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Està mig ennuvolat.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-cloudy.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Cloudy</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Està ennuvolat.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-fog.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Fog</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Hi ha boira o el cel cobert.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-showers.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Rain</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Està plovent o hi ha humitat.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-snow.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Snow</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Està nevant.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-storm.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Storm</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Hi ha tempesta.</para></entry>
             </row>
       </tbody>
      </tgroup>
     </informaltable>
    </sect2>

    <sect2 id="gweather-introduction-add">
      <title>Com afegir el butlletí meteorològic al quadre</title>
      <para>Per a afegir el <application>butlletí meteorològic</application> al quadre, feu clic amb el botó secundari sobre el quadre i trieu <guimenuitem>Afegeix al quadre</guimenuitem>. En el diàleg <application>Afegeix al quadre</application> seleccioneu <application>Butlletí meteorològic</application> i després feu clic a <guibutton>Afegeix</guibutton>.</para>
    </sect2>

      
   </sect1>
   <!-- ============= Usage ===================================== -->
   <sect1 id="gweather-usage">
      <title>Preferències</title>
      <para lang="en">
       The preferences dialog is accessed by right-clicking on the Weather Report in
       the panel.
      <figure id="weather-applet-menu-prefs">
	<title lang="en">Weather Report menu</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-menu-prefs.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Context menu</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
      </para>
      <sect2 id="gweather-change-location">
         <title>Com canviar a una ubicació determinada</title>
      <figure id="weather-applet-prefs-locations-fig">
	<title>Preferències de la ubicació</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-prefs-locations.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Location Preferences</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
         <para>Quan afegiu el <application>butlletí meteorològic</application> per primer cop al quadre, l'aplicació mostra el temps de Pittsburg, Pennsilvània, per defecte. Per a visualitzar el temps d'una ubicació diferent, realitzeu els passos següents:</para>
         <orderedlist>
            <listitem>
               <para>Feu clic amb el botó secundari sobre l'aplicació.</para>
            </listitem>
            <listitem>
               <para>Trieu <guimenuitem>Preferències</guimenuitem> al menú emergent de l'aplicació. L'aplicació mostrarà el diàleg <guilabel>Preferències del butlletí meteorològic</guilabel>.</para>
            </listitem>
            <listitem>
               <para>Seleccioneu la pestanya <guilabel>Ubicació</guilabel>. La pestanya <guilabel>Ubicació</guilabel> conté una llista de les regions geogràfiques, les subregions i ubicacions específiques.</para>
            </listitem>
            <listitem>
               <para>Feu clic a la fletxa situada al costat d'una regió per a veure'n les subregions.</para>
            </listitem>
            <listitem>
               <para>Feu clic a la fletxa situada al costat d'una subregió per a veure'n les ubicacions.</para>
            </listitem>
            <listitem>
               <para>Feu clic a una ubicació. Mentre l'aplicació obté la informació del temps per a la ubicació nova, l'indicador de funció «S'està actualitzant» es mostra quan apunteu a la icona.</para>
            </listitem>
            <listitem>
               <para>Feu clic a <guibutton>Tanca</guibutton> per a sortir del diàleg <guilabel>Preferències del butlletí meteorològic</guilabel>.</para>
            </listitem>
         </orderedlist>
	 <para>Podeu provar la cerca introduint el nom de la vostra ciutat al camp <guilabel>Cerca</guilabel>. Fixeu-vos que per ciutats molt properes o ciutats sense aeroport, és possible que hàgiu de triar una ciutat propera de la regió.</para>
      </sect2>
      <sect2 id="gweather-update">
         <title>Com actualitzar la informació del temps</title>
         <para>Per a actualitzar la informació del temps que el butlletí meteorològic mostra al quadre, feu clic amb el botó secundari sobre la icona, i trieu <guimenuitem>Actualitza</guimenuitem>.</para>
         <para>Per a actualitzar automàticament la informació del temps a intervals regulars, realitzeu els passos següents:</para>
	    <orderedlist>
                  <listitem><para>Aneu al menú fent clic amb el botó secundari i seleccioneu <guimenuitem>Preferències</guimenuitem>.</para></listitem>
                   <listitem><para>Al diàleg <guilabel>Preferències del butlletí meteorològic</guilabel> de la pestanya <guilabel>General</guilabel>, seleccioneu l'opció <guilabel>Actualitza automàticament cada ... minuts</guilabel>.</para></listitem>
		<listitem><para>Utilitzeu el quadre de selecció de valors per a especificar l'interval de temps en què el butlletí meteorològic obtindrà informació actualitzada del servidor del temps. El valor predeterminat és una comprovació cada trenta minuts.</para></listitem>
                    <listitem><para>Feu clic a <guibutton>Tanca</guibutton> per a sortir del diàleg <guilabel>Preferències del butlletí meteorològic</guilabel>.</para></listitem>
	      </orderedlist>
         </sect2>
        <sect2 id="gweather-metric"> 
               <title>Com canviar les unitats</title>
      <figure id="weather-applet-prefs-general-fig">
	<title>Preferències generals</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-prefs-general.png" format="PNG"/>
               </imageobject>
               <textobject>
                  <phrase>General Preferences</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
		     <para>Aneu al menú fent clic amb el botó secundari i seleccioneu <guimenuitem>Preferències</guimenuitem>. En el diàleg <guilabel>Preferències del butlletí meteorològic</guilabel> i a la pestanya <guilabel>General</guilabel>, seleccioneu les unitats de mesura que voleu utilitzar.</para>
		     <para>Hi ha diferents mesures disponibles, incloent-hi la mètrica, la imperial, el SI i d'altres. L'opció <guilabel>Per defecte</guilabel> utilitzarà el que es creu que és el valor predeterminat de la vostra regió, basat en la vostra configuració local.</para>
          </sect2>
         
   </sect1>
   <!-- ============= Preferences ================================== -->
   <sect1 id="gweather-settings">
      <title>Detalls</title>

         <para>Per a veure informació detallada del temps, feu clic amb el botó secundari sobre el butlletí meteorològic i trieu <guimenuitem>Detalls</guimenuitem>. El butlletí meteorològic mostrarà el diàleg <guilabel>Detalls</guilabel>. Per defecte el diàleg <guilabel>Detalls</guilabel> conté les pestanyes següents:</para>
         <itemizedlist>
            <listitem>
               <para lang="en">
                  <guilabel>Current Conditions</guilabel>
               </para>
            </listitem>
            <listitem>
               <para lang="en">
                  <guilabel>Forecast</guilabel>
               </para>
            </listitem>
            <listitem>
               <para lang="en">
                  <guilabel>Radar Map (optional)</guilabel>
               </para>
            </listitem>
         </itemizedlist>
         <sect2 id="gweather-current-conditions">
            <title>Condicions actuals</title>
      <figure id="weather-applet-details-fig">
	<title>Detalls del butlletí meteorològic</title>  		
         <screenshot>             		  
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-details.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Weather Report details</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
            <para>La pestanya <guilabel>Condicions actuals</guilabel> mostra la informació següent:</para>
            <informaltable>
               <tgroup cols="2">
                  <colspec colname="col1" colwidth="0.81*"/><colspec colname="col2" colwidth="1.19*"/>
                  <tbody>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Ciutat</para>
                        </entry>
                        <entry colname="col2">
                           <para>Ubicació on les condicions actuals del temps són vàlides.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Última actualització</para>
                        </entry>
                        <entry colname="col2">
                           <para>Data i hora de l'última actualització del temps al servidor meteorològic.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Condicions</para>
                        </entry>
                        <entry colname="col2">
                           <para>Estat del temps.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Cel</para>
                        </entry>
                        <entry colname="col2">
                           <para>Estat del cel.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Temperatura</para>
                        </entry>
                        <entry colname="col2">
                           <para>Temperatura actual.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Punt de rosada</para>
                        </entry>
                        <entry colname="col2">
                           <para>Temperatura a la qual es forma la rosada.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Humitat relativa</para>
                        </entry>
                        <entry colname="col2">
                           <para>Percentatge d'humitat a l'atmosfera.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Vent</para>
                        </entry>
                        <entry colname="col2">
                           <para>Direcció i velocitat del vent.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Pressió</para>
                        </entry>
                        <entry colname="col2">
                           <para>Pressió atmosfèrica.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Visibilitat</para>
                        </entry>
                        <entry colname="col2">
                           <para>Visibilitat determinada per la llum i les condicions de l'atmosfera.</para>
                        </entry>
                     </row>
		     <row valign="top">
		      <entry colname="col1">
		       <para>Sortida del sol</para>
		      </entry>
		      <entry colname="col2">
		       <para>L'hora calculada de la sortida del sol per a la vostra ubicació</para>
		      </entry>
		     </row>
		     <row valign="top">
		      <entry colname="col1">
		       <para>Posta del sol</para>
		      </entry>
		      <entry>
		       <para>L'hora calculada de la posta de sol per a la vostra ubicació</para>
		      </entry>
		     </row>
                  </tbody>
               </tgroup>
            </informaltable>

	    <note><para>La sortida i la posta del sol es calculen a nivell local amb la latitud i la longitud de la informació emmagatzemada al vostre ordinador. Algunes condicions, com ara la refracció de la llum a través de l'aire, són difícils de modelar. Per aquests motius, els valors calculats per a la sortida i la posta del sol poden estar desfasats fins a 10 minuts.</para></note>
         </sect2>
         <sect2 id="gweather-forecast">
            <title>Pronòstic</title>
            <para>La pestanya <guilabel>Previsió</guilabel> mostra una previsió de la ubicació per al futur immediat, normalment per als propers cinc dies.</para>
            <note>
               <para>Les previsions només estan disponibles per algunes localitats als EUA, Austràlia i el Regne Unit.</para>
            </note>
         </sect2>
         <sect2 id="gweather-radar-map">
            <title>Mapa de radar</title>
            <para>Per defecte, la pestanya del <guilabel>Mapa de radar</guilabel> no es mostra al diàleg <guilabel>Previsió</guilabel>. El <application>butlletí meteorològic</application> baixa els mapes de radar de www.weather.com. Si el mapa de radar no està disponible a www.weather.com, el butlletí meteorològic mostrarà un signe d'interrogació. Per anar a la pàgina web www.weather.com, feu clic al botó <guibutton>Visita Weather.com</guibutton>.</para>
            <para lang="en">To enable the radar map, perform the following steps:
	     <orderedlist>
		<listitem><para lang="en">Go to the <guilabel>Weather Preferences</guilabel> dialog by selecting <guilabel>Preferences</guilabel> in the right-click menu.</para></listitem>
 		<listitem><para lang="en">In the <guilabel>General</guilabel> tab, select the <guilabel>Enable radar map</guilabel> option. </para></listitem>
		<listitem><para lang="en">By default, <application>Weather Report</application> downloads the radar maps from www.weather.com. Select the <guilabel>Use custom address for radar map</guilabel> option to display radar maps from an alternative Internet address. You must type the address in the <guilabel>Address</guilabel> text box.</para></listitem>
	      </orderedlist>
                     </para>
	<note><para>La majoria de les ubicacions no defineixen un mapa de radar predeterminat, especialment fora dels EUA. A moltes ubicacions caldrà que hi especifiqueu un URL personalitzat, si és que voleu tenir un mapa de radar. Per exemple, l'adreça del radar del Servei Meteorològic de Catalunya és: http://www.meteo.cat/mediamb_xemec/servmet/radar/images/cappi250_catalunya_10dBZ/composicio_ultima.png</para></note>
      </sect2>
   </sect1>
</article>
