<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.12">
<!ENTITY applet "Weather Report">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="el">
   <!-- please do not change the id; for translations, change lang to -->
   <!-- appropriate code -->
     
   <articleinfo>
      <title>Εγχειρίδιο δελτίου καιρού</title>
      <abstract role="description">
	<para>Το εφαρμογίδιο Δελτίο καιρού μεταφέρει από το διαδίκτυο μετεωρολογικές πληροφορίες για μια δεδομένη τοποθεσία και εμφανίζει τη θερμοκρασία και ένα σύμβολο που αναπαριστά τις τρέχουσες καιρικές συνθήκες στον πίνακα εφαρμογών. Όταν γίνεται κλικ επάνω του, εμφανίζονται περισσότερες πληροφορίες, όπως πρόγνωση, ώρα ανατολής και δύσης ηλίου, κατεύθυνση ανέμου, πίεση κ.ά. Όλες οι μονάδες μέτρησης μπορούν να καθορισθούν από το χρήστη.</para>
      </abstract>
      <copyright lang="en">
        <year>2005</year>
	<holder>Davyd Madeley</holder>
      </copyright>
      <copyright lang="en">
	<year>2004</year>
	<holder>Angela Boyle</holder>
      </copyright>
      <copyright lang="en">
		<year>2002</year>
		<year>2003</year>
		<year>2004</year>
		<holder>Sun Microsystems</holder>    	 
      </copyright>
      <copyright lang="en">
		<year>1999</year>  		
		<year>2000</year>
		<holder>Spiros Papadimitriou</holder>  	 
      </copyright>
      <copyright lang="en">
		<year>1999</year>  		
		<year>2000</year>
		<holder>Dan Mueth</holder>  	 
      </copyright>
      <!-- translators: uncomment this:
      
        <copyright>
         <year>2002</year>
         <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
        </copyright>
      
         -->
      <!-- An address can be added to the publisher information.  If a role is 
           not specified, the publisher/author is the same for all versions of the 
           document.  -->
      	 
      <publisher role="maintainer">
		<publishername>GNOME Documentation Project</publishername>  	 
      </publisher>
        
      <!-- This file  contains link to license for the documentation (GNU FDL), and 
           other legal stuff such as "NO WARRANTY" statement. Please do not change 
           any of this. -->
      	 
      <authorgroup>
        <author lang="en">
	  <firstname>Davyd</firstname><surname>Madeley</surname>
	  <affiliation>
	   <orgname>GNOME Project</orgname>
	   <address><email>davyd@madeley.id.au</email></address>
	  </affiliation>
	</author>
	<author lang="en">
	  <firstname>Angela</firstname>
	  <surname>Boyle</surname>
	</author>
           		
	<author lang="en">
	<firstname>Sun </firstname>  		  
	<surname>GNOME Documentation Team</surname>            		  

            <affiliation>
		<orgname>Sun Microsystems</orgname>  			 
            </affiliation>
              		
        </author>
           		
        <author lang="en">
	<firstname>Spiros</firstname>  		  
	<surname>Papadimitriou</surname>
             		  
            <affiliation>
		<orgname>GNOME Documentation Project</orgname>  			 
		<address><email>spapadim+@cs.cmu.edu</email> </address>  		  
            </affiliation>
              		
         </author>
           		
         <author lang="en">
	 <firstname>Dan</firstname>
	 <surname>Mueth</surname>  	
	  
	     <affiliation>
 		<orgname>GNOME Documentation Project</orgname>  			 
		<address><email>d-mueth@uchicago.edu</email> </address>  		  
             </affiliation>
              		
         </author>
               
         <!-- This is appropriate place for other contributors: translators,
                    maintainers,  etc. Commented out by default.
                    <othercredit role="translator">
         	     <firstname>Latin</firstname> 
         	     <surname>Translator 1</surname> 
         	     <affiliation> 
         	       <orgname>Latin Translation Team</orgname> 
         	       <address> <email>translator@gnome.org</email> </address> 
         	     </affiliation>
         	     <contrib>Latin translation</contrib>
                    </othercredit>
         -->
         	 
      </authorgroup>
	  
	  <releaseinfo revision="2.26" role="review"/>
      	 
      <revhistory>
        <revision lang="en">
	  <revnumber>Version 2.12</revnumber>
	  <date>March 2005</date>
	  <revdescription>
	    <para role="author" lang="en">Davyd Madeley</para>
	  </revdescription>
	</revision>
        <revision lang="en">
	  <revnumber>Version 2.10</revnumber>
	  <date>March 2005</date>
	  <revdescription>
	    <para role="author" lang="en">Davyd Madeley</para>
	  </revdescription>
	</revision>
	<revision lang="en">
	  <revnumber>Version 2.8</revnumber>
	  <date>September 2004</date>
	  <revdescription>
	    <para role="author" lang="en">Angela Boyle</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
	  </revdescription>
	</revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.4</revnumber>  		
          <date>February 2004</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.3</revnumber>  		
          <date>January 2003</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>  
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.2</revnumber>  		
          <date>August 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>  
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.1</revnumber>  		
          <date>July 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
          <revnumber>Weather Report Applet Manual V2.0</revnumber>  		
          <date>March 2002</date>  		  
          <revdescription>
	    <para role="author" lang="en">Sun GNOME Documentation Team</para>
	    <para role="publisher" lang="en">GNOME Documentation Project</para>
          </revdescription>
        </revision>
        <revision lang="en">
	  <revnumber>GNOME Weather Applet</revnumber>  		  
	  <date>2000</date>
          <revdescription>
            <para role="author" lang="en">
              Spiros Papadimitriou 				
              <email>spapadim+@cs.cmu.edu</email>
            </para>
            <para role="author" lang="en">
              Dan Mueth 				
	      <email>d-mueth@uchicago.edu</email>
            </para>
            <para role="publisher" lang="en">
              GNOME Documentation Project
            </para>
          </revdescription>
        </revision>
      </revhistory>
        	 
      <releaseinfo>Αυτό το εγχειρίδιο περιγράφει την έκδοση 2.12 του Δελτίου καιρού.</releaseinfo>
        	 
      <legalnotice>
         <title>Ανάδραση</title>  		
         <para>Για να αναφέρετε ένα πρόβλημα ή να κάνετε μια πρόταση σχετικά με το Δελτίο καιρού ή αυτό το εγχειρίδιο, ακολουθήστε τις οδηγίες στο <ulink url="ghelp:gnome-feedback" type="help">GNOME Feedback Page</ulink>.</para>
         		 	 
      </legalnotice>
          
   </articleinfo>
       <indexterm lang="en">  	 
       <primary>Weather Report</primary>    
       </indexterm> 
   <!-- ============= Document Body ============================= -->
   <!-- ============= Introduction ============================== -->
   <sect1 id="gweather-introduction">
      <title>Εισαγωγή</title>
      <!-- ==== Figure ============================================= -->
      	 
      <figure id="weather-applet-fig">
	<title>Δελτίο καιρού</title>  		
         <screenshot>             		  
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather_applet.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Shows Weather Report. Contains a weather icon and current temperature</phrase> 			 
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
       
      <!-- ==== End of Figure ======================================= -->
      <para>Το <application>Δελτίο καιρού</application> μεταφέρει μετεωρολογικές πληροφορίες από εξυπηρετητές της Εθνικής Μετεωρολογικής Υπηρεσία των Ηνωμένων Πολιτειών (NWS), συμπεριλαμβανομένου του Δικτύου Διαδραστική Μετεωρολογικής Πληροφορίας (IWIN) και άλλες μετεωρολογικές υπηρεσίες. Μπορείτε να χρησιμοποιήσετε το <application>Δελτίο καιρού</application> για να εμφανίσετε τρέχουσες μετεωρολογικές πληροφορίες και προγνώσεις στον υπολογιστή σας.</para>
      <note id="proxies-note-1">
         <para>Αν ο υπολογιστής βρίσκεται πίσω από τείχος προστασίας, πρέπει να χρησιμοποιήσετε έναν proxy εξυπηρετητή για να έχετε πρόσβαση στις μετεωρολογικές πληροφορίες. Για να ρυθμίσετε την επιφάνεια εργασίας του GNOME να χρησιμοποιεί έναν proxy εξυπηρετητή, χρησιμοποιήστε τα εργαλεία των προτιμήσεων για να ορίσετε τον proxy εξυπηρετητή του δικτύου για τις συνδέσεις διαδικτύου.</para>
      </note>   
      <para>Το <application>Δελτίο καιρού</application> εμφανίζει τις ακόλουθες πληροφορίες για την προεπιλεγμένη τοποθεσία ή την τοποθεσία που θα ορίσετε:</para>
      <itemizedlist>
         <listitem>
            <para>Ένα εικονίδιο καιρού που αναπαριστά τις γενικές καιρικές συνθήκες. Βλέπε <xref linkend="gweather-introduction-icons"/>.</para>
         </listitem>
         <listitem>
            <para>Την τρέχουσα θερμοκρασία.</para>
         </listitem>
      </itemizedlist>

    <sect2 id="gweather-introduction-icons">
     <title>Εικονίδια καιρού στον πίνακα εφαρμογών</title>
     <informaltable frame="all">
      <tgroup cols="2" colsep="1" rowsep="1">
       <colspec colname="COLSPEC0" colwidth="50*"/>
       <colspec colname="COLSPEC1" colwidth="50*"/>
       <thead>
        <row valign="top">
	 <entry colname="COLSPEC0"><para>Εικονίδιο</para></entry>
	 <entry colname="COLSPEC1"><para>Περιγραφή</para></entry>
	</row>
       </thead>
       <tbody>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-sunny.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Sunny</phrase>
	      </textobject></inlinemediaobject>
	      <inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-night-clear.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Night</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Αίθριος.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-few-clouds.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Few Clouds</phrase>
	      </textobject></inlinemediaobject>
	      <inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-night-few-clouds.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Night Few Clouds</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Λίγη συννεφιά.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-cloudy.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Cloudy</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Είναι συννεφιασμένος.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-fog.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Fog</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Ομιχλώδης</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-showers.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Rain</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Βροχερός ή υγρός.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-snow.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Snow</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Χιονίζει.</para></entry>
             </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
	       <imagedata fileref="figures/stock_weather-storm.png" format="PNG"/>
	      </imageobject><textobject>
	       <phrase>Stock Storm</phrase>
	      </textobject></inlinemediaobject></para></entry>
              <entry><para>Έχει καταιγίδα.</para></entry>
             </row>
       </tbody>
      </tgroup>
     </informaltable>
    </sect2>

    <sect2 id="gweather-introduction-add">
      <title>Προσθήκη Δελτίου καιρού σε πίνακα εφαρμογών</title>
      <para>Για να προσθέσετε το <application>Δελτίο καιρού</application> σε έναν πίνακα εφαρμογών, κάντε δεξί κλικ στον πίνακα εφαρμογών και επιλέξτε <guimenuitem>Προσθήκη στον πίνακα εφαρμογών</guimenuitem>. Επιλέξτε το <application>Δελτίο καιρού</application> στο παράθυρο διαλόγου <application>Προσθήκη στον πίνακα εφαρμογών</application> και μετά πατήστε το <guibutton>OK</guibutton>.</para>
    </sect2>

      
   </sect1>
   <!-- ============= Usage ===================================== -->
   <sect1 id="gweather-usage">
      <title>Προτιμήσεις</title>
      <para lang="en">
       The preferences dialog is accessed by right-clicking on the Weather Report in
       the panel.
      <figure id="weather-applet-menu-prefs">
	<title lang="en">Weather Report menu</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-menu-prefs.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Context menu</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
      </para>
      <sect2 id="gweather-change-location">
         <title>Αλλαγή σε συγκεκριμένη τοποθεσία</title>
      <figure id="weather-applet-prefs-locations-fig">
	<title>Προτιμήσεις τοποθεσίας</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-prefs-locations.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Location Preferences</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
         <para>Όταν προσθέσετε το <application>Δελτίο καιρού</application> στον πίνακα εφαρμογών για πρώτη φορά, η εφαρμογή εμφανίζει τον καιρό στο Πίτσμπουργκ της Πεννσυλβάνια (προεπιλογή). Για να εμφανίζεται ο καιρός για άλλη τοποθεσία, κάντε τα ακόλουθα:</para>
         <orderedlist>
            <listitem>
               <para>Δεξί κλικ στην εφαρμογή.</para>
            </listitem>
            <listitem>
               <para>Επιλέξετε <guimenuitem>Προτιμήσεις</guimenuitem> από το αναδυόμενο μενού. Η εφαρμογή εμφανίζει το παράθυρο διαλόγου <guilabel>Προτιμήσεις Δελτίου καιρού</guilabel>.</para>
            </listitem>
            <listitem>
               <para>Επιλέξτε την καρτέλα <guilabel>Τοποθεσία</guilabel>. Η ενότητα <guilabel>Τοποθεσία</guilabel> περιλαμβάνει μια λίστα γεωγραφικών περιοχών, υποπεριοχών και συγκεκριμένων τοποθεσιών.</para>
            </listitem>
            <listitem>
               <para>Κάντε κλικ στο βέλος δίπλα σε μια περιοχή για να εμφανίσετε τις υποπεριοχές της.</para>
            </listitem>
            <listitem>
               <para>Κάντε κλικ στο βέλος δίπλα σε μια υποπεριοχή για να εμφανίσετε τις τοποθεσίες της.</para>
            </listitem>
            <listitem>
               <para>Κάντε κλικ σε μια τοποθεσία. Ενόσω η εφαρμογή ανακτά τις μετεωρολογικές πληροφορίες για τη νέα τοποθεσία, εμφανίζεται "Ενημέρωση" όταν ο δείκτης είναι πάνω στο εικονίδιο.</para>
            </listitem>
            <listitem>
               <para>Κάντε κλικ στο <guibutton>Κλείσιμο</guibutton> για κλείσετε το παράθυρο διαλόγου <guilabel>Προτιμήσεις Δελτίου καιρού</guilabel>.</para>
            </listitem>
         </orderedlist>
	 <para>Μπορείτε να δοκιμάσετε να ψάξετε εισάγοντας το όνομα της πόλης σας στο πεδίο <guilabel>Εύρεση</guilabel>. Έχετε υπόψιν ότι για κοντινές πόλεις ή για πόλεις χωρίς αεροδρόμιο μπορεί να χρειαστεί να επιλέξετε μια κοντινή πόλη της περιοχής.</para>
      </sect2>
      <sect2 id="gweather-update">
         <title>Ενημέρωση πληροφοριών καιρού</title>
         <para>Για να ενημερώσετε τις μετεωρολογικές πληροφορίες που εμφανίζει το Δελτίο καιρού στον πίνακα εφαρμογών, κάντε δεξί κλικ στο εικονίδιο και επιλέξτε <guimenuitem>Ενημέρωση</guimenuitem>.</para>
         <para>Για να ανανεώνετε τις πληροφορίες καιρού σε τακτά διαστήματα, ακολουθήστε τα παρακάτω βήματα:</para>
	    <orderedlist>
                  <listitem><para>Με δεξί κλικ εμφανίστε το μενού και επιλέξτε <guimenuitem>Προτιμήσεις</guimenuitem>.</para></listitem>
                   <listitem><para>Στο παράθυρο διαλόγου <guilabel>Προτιμήσεις καιρού</guilabel> της καρτέλας <guilabel>Γενικά</guilabel>, επιλέξτε το <guilabel>Αυτόματη ενημέρωση κάθε ... λεπτά</guilabel>.</para></listitem>
		<listitem><para>Χρησιμοποιήστε το πεδίο για να καθορίσετε το χρονικό διάστημα στο οποίο το Δελτίο καιρού θα λαμβάνει ανανεωμένες πληροφορίες από τον εξυπηρετητή καιρού. Η προεπιλογή είναι να γίνεται έλεγχος κάθε τριάντα λεπτά.</para></listitem>
                    <listitem><para>Κάντε κλικ στο <guibutton>Κλείσιμο</guibutton> για κλείσετε το παράθυρο διαλόγου <guilabel>Προτιμήσεις Δελτίου καιρού</guilabel>.</para></listitem>
	      </orderedlist>
         </sect2>
        <sect2 id="gweather-metric"> 
               <title>Αλλαγή μονάδων</title>
      <figure id="weather-applet-prefs-general-fig">
	<title>Γενικές προτιμήσεις</title>
         <screenshot>
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-prefs-general.png" format="PNG"/>
               </imageobject>
               <textobject>
                  <phrase>General Preferences</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
		     <para>Κάντε δεξί κλικ και στο μενού επιλέξτε <guimenuitem>Προτιμήσεις</guimenuitem>. Στο παράθυρο διαλόγου <guilabel>Προτιμήσεις καιρού</guilabel> της καρτέλας <guilabel>Γενικά</guilabel>, επιλέξτε τις μονάδες μέτρησεις που θέλετε να χρησιμοποιήσετε.</para>
		     <para>Είναι διαθέσιμα διάφορα συστήματα μέτρησης, μεταξύ των οποίων το μετρικό, το αγγλοσαξωνικό, το SI και άλλα. Η επιλογή <guilabel>Προεπιλογή</guilabel> θα χρησιμοποιήσει αυτό που νομίζουμε ότι είναι το κανονικό στην περιοχή σας, βασισμένο στην επιλογή τοποθεσίας σας.</para>
          </sect2>
         
   </sect1>
   <!-- ============= Preferences ================================== -->
   <sect1 id="gweather-settings">
      <title>Λεπτομέρειες</title>

         <para>Για να δείτε λεπτομερείς μετεωρολογικές πληροφορίες, κάντε δεξί κλικ στο Δελτίο καιρού και επιλέξτε <guimenuitem>Λεπτομέρειες</guimenuitem>. Το Δελτίο καιρού εμφανίζει το παράθυρο διαλόγου <guilabel>Λεπτομέρειες</guilabel>. Αυτό το παράθυρο περιέχει τις ακόλουθες προεπιλεγμένες καρτέλες:</para>
         <itemizedlist>
            <listitem>
               <para lang="en">
                  <guilabel>Current Conditions</guilabel>
               </para>
            </listitem>
            <listitem>
               <para lang="en">
                  <guilabel>Forecast</guilabel>
               </para>
            </listitem>
            <listitem>
               <para lang="en">
                  <guilabel>Radar Map (optional)</guilabel>
               </para>
            </listitem>
         </itemizedlist>
         <sect2 id="gweather-current-conditions">
            <title>Τρέχουσες συνθήκες</title>
      <figure id="weather-applet-details-fig">
	<title>Λεπτομέρειες Δελτίου καιρού</title>  		
         <screenshot>             		  
            <mediaobject lang="en">                 			 
               <imageobject>
                  <imagedata fileref="figures/gweather-details.png" format="PNG"/>                  			 
               </imageobject>
               <textobject>
                  <phrase>Weather Report details</phrase>
               </textobject>
            </mediaobject>
         </screenshot>
      </figure>
            <para>Η καρτέλα <guilabel>Τρέχουσες συνθήκες</guilabel> εμφανίζει τις ακόλουθες πληροφορίες:</para>
            <informaltable>
               <tgroup cols="2">
                  <colspec colname="col1" colwidth="0.81*"/><colspec colname="col2" colwidth="1.19*"/>
                  <tbody>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Πόλη</para>
                        </entry>
                        <entry colname="col2">
                           <para>Η τοποθεσία για την οποία εμφανίζονται οι τρέχουσες καιρικές συνθήκες.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Τελευταία ενημέρωση</para>
                        </entry>
                        <entry colname="col2">
                           <para>Ο χρόνος κατά τον οποίο έγινε η τελευταία ενημέρωση από τον μετεωρολογικό εξυπηρετητή.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Συνθήκες</para>
                        </entry>
                        <entry colname="col2">
                           <para>Γενικές καιρικές συνθήκες.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Ουρανός</para>
                        </entry>
                        <entry colname="col2">
                           <para>Γενική κατάσταση ουρανού.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Θερμοκρασία</para>
                        </entry>
                        <entry colname="col2">
                           <para>Τρέχουσα θερμοκρασία.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Σημείο δρόσου</para>
                        </entry>
                        <entry colname="col2">
                           <para>Η θερμοκρασία στην οποία θα σχηματισθεί δρόσος.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Yγρασία</para>
                        </entry>
                        <entry colname="col2">
                           <para>Το ποσοστό της υγρασίας στην ατμόσφαιρα.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Άνεμος</para>
                        </entry>
                        <entry colname="col2">
                           <para>Διεύθυνση και ταχύτητα του ανέμου.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Πίεση</para>
                        </entry>
                        <entry colname="col2">
                           <para>Ατμοσφαιρική πίεση.</para>
                        </entry>
                     </row>
                     <row valign="top">
                        <entry colname="col1">
                           <para>Ορατότητα</para>
                        </entry>
                        <entry colname="col2">
                           <para>Η ορατή απόσταση όπως καθορίζεται από το φως και τις ατμοσφαιρικές συνθήκες.</para>
                        </entry>
                     </row>
		     <row valign="top">
		      <entry colname="col1">
		       <para>Ανατολή</para>
		      </entry>
		      <entry colname="col2">
		       <para>Η υπολογισμένη ώρα ανατολής του ηλίου για την τοποθεσία σας.</para>
		      </entry>
		     </row>
		     <row valign="top">
		      <entry colname="col1">
		       <para>Δύση</para>
		      </entry>
		      <entry>
		       <para>Η υπολογισμένη ώρα δύσης του ηλίου για την τοποθεσία σας.</para>
		      </entry>
		     </row>
                  </tbody>
               </tgroup>
            </informaltable>

	    <note><para>Οι ώρες ανατολής και δύσης του ηλίου υπολογίζονται τοπικά από τις πληροφορίες για το γεωγραφικό πλάτος και μήκος που είναι αποθηκευμένες στον υπολογιστή σας. Μερικές συνθήκες όπως η διάθλαση του φωτός στον αέρα είναι δύσκολο να μοντελοποιηθούν. Συνεπακόλουθα, οι υπολογισμένες ώρες ανατολής και δύσης μπορεί να αποκλίνουν μέχρι και 10 λεπτά.</para></note>
         </sect2>
         <sect2 id="gweather-forecast">
            <title>Πρόγνωση</title>
            <para>Η καρτέλα <guilabel>Πρόγνωση</guilabel> εμφανίζει μια πρόγνωση καιρού για την τοποθεσία για το άμεσο μέλλον, συνήθως τις επόμενες πέντε ημέρες.</para>
            <note>
               <para>Οι προγνώσεις είναι διαθέσιμες μόνο για ορισμένες τοποθεσίες στις Η.Π.Α., την Αυστραλία και το Ηνωμένο Βασίλειο.</para>
            </note>
         </sect2>
         <sect2 id="gweather-radar-map">
            <title>Χάρτης ραντάρ</title>
            <para>Ως προεπιλογή η καρτέλα <guilabel>Χάρτης ραντάρ</guilabel> δεν εμφανίζεται στο παράθυρο διαλόγου <guilabel>Πρόγνωση</guilabel>. Το <application>Δελτίο καιρού</application> μεταφορτώνει τους χάρτες ραντάρ από το www.weather.com. Αν ο χάρτης ραντάρ δεν διαθέσιμος από αυτόν τον ιστότοπο, το Δελτίο καιρού εμφανίζει ένα ερωτηματικό. Για να μεταβείτε στην ιστοσελίδα του www.weather.com, κάντε κλικ στο κουμπί <guibutton>Επισκεφθείτε το Weather.com</guibutton>.</para>
            <para lang="en">To enable the radar map, perform the following steps:
	     <orderedlist>
		<listitem><para lang="en">Go to the <guilabel>Weather Preferences</guilabel> dialog by selecting <guilabel>Preferences</guilabel> in the right-click menu.</para></listitem>
 		<listitem><para lang="en">In the <guilabel>General</guilabel> tab, select the <guilabel>Enable radar map</guilabel> option. </para></listitem>
		<listitem><para lang="en">By default, <application>Weather Report</application> downloads the radar maps from www.weather.com. Select the <guilabel>Use custom address for radar map</guilabel> option to display radar maps from an alternative Internet address. You must type the address in the <guilabel>Address</guilabel> text box.</para></listitem>
	      </orderedlist>
                     </para>
	<note><para>Οι περισσότερες τοποθεσίες δεν καθορίζουν κάποιο προεπιλεγμένο χάρτη ραντάρ, ιδίως εκτός των Η.Π.Α. Για πολλές τοποθεσίες θα πρέπει να ορίσετε εσείς μια κατάλληλη URL, αν επιθυμείτε να έχετε χάρτη ραντάρ.</para></note>
      </sect2>
   </sect1>
</article>
