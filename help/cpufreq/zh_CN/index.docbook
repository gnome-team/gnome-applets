<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.12.0">
<!ENTITY applet "CPU Frequency Scaling Monitor">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 06, 2003
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="zh-CN">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
	<title>CPU 频率调节监视器手册</title> 
        <abstract role="description">
	  <para>CPU 频率调节监视器会显示 CPU 的当前时钟频率，在 CPU 支持时您还可以更改其时钟频率。</para>
	</abstract>
	<copyright lang="en">
	  <year>2005</year>
	  <holder>Davyd Madeley</holder>
	</copyright>
        <copyright lang="en">
	   <year>2004</year>
            <holder>GNOME Foundation</holder>
        </copyright>

<!-- translators: uncomment this:
  <copyright>
   <year>2003</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>
   -->

	 <publisher role="maintainer"> 
		<publishername>GNOME 文档计划</publishername> 
	 </publisher> 


<!-- This file  contains link to license for the documentation (GNU FDL), and 
     other legal stuff such as "NO WARRANTY" statement. Please do not change 
     any of this. -->

	 <authorgroup> 
            <author lang="en">
	       <firstname>Carlos</firstname>
	       <surname>Garcia Campos</surname>
	       <affiliation>
                  <orgname>GNOME Project</orgname>
		  <address><email>carlosgc@gnome.org</email></address>
	       </affiliation>
            </author>
	    <author lang="en">
	      <firstname>Davyd</firstname>
	      <surname>Madeley</surname>
	      <affiliation>
	        <orgname>GNOME Project</orgname>
		<address><email>davyd@madeley.id.au</email></address>
	      </affiliation>
	    </author>
      <!-- This is appropriate place for other contributors: translators,
           maintainers,  etc. Commented out by default.
        <othercredit role="translator">
	      <firstname>Latin</firstname> 
	      <surname>Translator 1</surname> 
	      <affiliation> 
	      	<orgname>Latin Translation Team</orgname> 
	       	<address> <email>translator@gnome.org</email> </address> 
	      </affiliation>
	      <contrib>Latin translation</contrib>
        </othercredit>
-->
	 </authorgroup>
	 
	 <releaseinfo revision="2.26" role="review"/>

	 <revhistory>
	   <revision lang="en">
	     <revnumber>GNOME 2.12</revnumber>
	     <date>August 2005</date>
	     <revdescription>
	      <para role="author" lang="en">Davyd Madeley</para>
	     </revdescription>
	   </revision>
	   <revision lang="en">
	     <revnumber>GNOME 2.10</revnumber>
	     <date>March 2005</date>
	     <revdescription>
	      <para role="author" lang="en">Davyd Madeley</para>
	     </revdescription>
	   </revision>
		<revision lang="en"> 
		  <revnumber>CPU Frequency Scaling Monitor Applet Manual V0.3</revnumber> 
		  <date>October 2004</date> 
		  <revdescription> 
                    <para role="author" lang="en">Carlos Garcia Campos</para>
                    <para role="publisher" lang="en">GNOME Documentation Project</para>
		  </revdescription> 
		</revision> 
	 </revhistory> 

	 <releaseinfo>此手册描述了 CPU 频率调节监视器的 2.12.0 版本。</releaseinfo> 

		<legalnotice> 
		<title>反馈</title> 
		<para>要报告关于 CPU 频率调节监视器小程序或本手册的错误或提出建议，请遵循 <ulink url="ghelp:gnome-feedback" type="help">GNOME 反馈页</ulink>中的指导。</para>
	     </legalnotice>

  </articleinfo> 

  <indexterm zone="index" lang="en"> 
	 <primary>CPU Frequency Scaling Monitor</primary> 
  </indexterm>
<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<sect1 id="cpufreq-applet-introduction"> 
	 <title>简介</title> 
	 <figure id="cpufreq-applet-figure"> 
	    <title>CPU 频率调节监视器</title> 
		<screenshot> 
		  <mediaobject> 
		     <imageobject>
			<imagedata fileref="figures/cpufreq-applet.png" format="PNG"/> 
		     </imageobject>
		  </mediaobject>
	       </screenshot>
	 </figure> 
    <!-- ==== End of Figure ======================================= -->
    <para><application>CPU 频率调节监视器</application> 允许您方便地监视每片 CPU 的频率调节。</para> 
	 <para lang="en">To add <application>CPU Frequency Scaling Monitor</application> to a panel, right-click on the panel to open the panel popup menu, then choose 
		<menuchoice> 
		  <guimenu>Add to Panel</guimenu> 
		  <guimenuitem>CPU Frequency Scaling Monitor</guimenuitem> 
		</menuchoice>.</para> 
	
    <note><para>不幸的是，CPU 频率调节只能在内核支持此功能的 Linux 计算机上进行监视。但是，内核自身还是支持几代 CPU 的频率调节的。</para></note>
   
   <para>如果系统不支持 CPU 频率调节，CPU 频率调节监视器将只能显示 CPU 的当前频率。</para>
   <para>如果系统支持 CPU 频率调节，CPU 频率调节监视器将显示带有进度栏的 CPU 图标。进度栏的状态代表了 CPU 的当前频率所占最高频率的百分比。</para>
   <para>默认情况下，CPU 频率调节监视器将显示以赫兹(频率的标准度量单位)为单位的 CPU 频率值(频率的标准度量单位)，但您可以对其进行配置以便显示百分比。要获得关于如何配置 CPU 频率调节监视器的更多信息，请参看<xref linkend="cpufreq-applet-prefs"/>。</para>
   <para>每个 CPU 频率调节监视器只能监视一片 CPU。如果您使用的是多处理器系统(SMP)，则必须为每片 CPU 启动一个 CPU 频率调节监视器。</para>

   <sect2 id="gnome-cpufreq-icons">
     <title>理解图标</title>
     <para>CPU 频率调节监视器使用一系列图标来代表 CPU 频率的当前调节级别。这些图标包括：</para>
      <informaltable frame="all">
        <tgroup cols="2" colsep="1" rowsep="1">
          <colspec colname="COLSPEC0" colwidth="50*"/>
          <colspec colname="COLSPEC1" colwidth="50*"/>
          <thead>
            <row valign="top">
              <entry colname="COLSPEC0">
                <para>图标</para></entry>
              <entry colname="COLSPEC1">
                <para>描述</para></entry>
            </row>
          </thead>
          <tbody>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
               <imagedata fileref="figures/cpufreq-100.png" format="PNG"/>
               </imageobject><textobject>
                <phrase>100% Power</phrase>
              </textobject></inlinemediaobject></para></entry>
	      <entry><para>CPU 正以 100% 或接近 100% 的频率运行</para></entry>
	    </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
               <imagedata fileref="figures/cpufreq-75.png" format="PNG"/>
               </imageobject><textobject>
                <phrase>75% Power</phrase>
              </textobject></inlinemediaobject></para></entry>
	      <entry><para>CPU 正以 75% 或接近 75% 的频率运行</para>
	      </entry>
	    </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
               <imagedata fileref="figures/cpufreq-50.png" format="PNG"/>
               </imageobject><textobject>
                <phrase>50% Power</phrase>
              </textobject></inlinemediaobject></para></entry>
	      <entry><para>CPU 正以 50% 或接近 50% 的频率运行</para></entry>
	    </row>
            <row valign="top">
              <entry><para lang="en"><inlinemediaobject><imageobject>
               <imagedata fileref="figures/cpufreq-25.png" format="PNG"/>
               </imageobject><textobject>
                <phrase>25% Power</phrase>
              </textobject></inlinemediaobject></para></entry>
	      <entry><para>CPU 正以 25% 或接近 25% 的频率运行</para></entry>
	    </row>
	  </tbody>
	</tgroup>
      </informaltable>
   </sect2>
   
   <sect2 id="gnome-cpufreq-selector">
      <title>频率和调速器选择器</title>
      <warning><para>频率选择器功能可能在您的 GNOME 上没有被默认开启。请咨询您的系统管理员，供应商文档或这此软件自带的文档。</para></warning>
      <para>要设定 CPU 频率，请用鼠标左键单击监视器，此时会出现一个菜单。此菜单允许您为您的计算机选择可用的频率或调速器。</para>
      <tip><para>您可以选择您是想要在<guilabel>首选项</guilabel>中显示频率还是调速器。更多信息请参看<xref linkend="cpufreq-applet-prefs"/>。</para></tip>

      <figure id="cpufreq-applet-selector-img">
	 <title>只显示频率的频率选择器</title>
	 <screenshot>
	    <mediaobject>
	       <imageobject>
		  <imagedata fileref="figures/cpufreq-applet-selector.png" format="PNG"/>
	       </imageobject>
	    </mediaobject>
	</screenshot>
      </figure>

      <figure id="cpufreq-applet-selector-both-img">
	 <title>显示频率和调速器的频率选择器</title>
	 <screenshot>
	    <mediaobject>
	       <imageobject>
		  <imagedata fileref="figures/cpufreq-applet-selector-both.png" format="PNG"/>
	       </imageobject>
	    </mediaobject>
	</screenshot>
      </figure>
   </sect2>
</sect1>

<!-- ============= Customization ============================= -->
<!-- Use this section to describe how to customize the applet. -->
<sect1 id="cpufreq-applet-prefs"> 
	 <title>首选项</title> 
	 <para>要配置 <application>CPU 频率调节监视器</application>，请用鼠标右键单击小程序，然后从弹出菜单中选择<guimenuitem>首选项</guimenuitem>。</para> 
	 <para><guimenuitem>首选项</guimenuitem>对话框包含如下组件：</para> 
	 <figure id="cpufreq-applet-preferences">
	    <title>CPU 频率调节监视器首选项对话框</title>
	    <screenshot>
	       <mediaobject lang="en">
		  <imageobject><imagedata fileref="figures/cpufreq-applet-preferences.png" format="PNG"/>
		  </imageobject>
		  <textobject>
		     <phrase>CPU Frequency Scaling Monitor preferences dialog</phrase>
		  </textobject>
	       </mediaobject>
	    </screenshot>
	 </figure>

	 <variablelist> 
	   <varlistentry> 
	      <term lang="en"><guilabel>Monitored CPU (only in multiprocessor systems)</guilabel></term> 
	     <listitem> 
		<para>使用此下拉列表可选择您想要监视的 CPU。</para>
	 <figure id="cpufreq-applet-preferences-smp">
	    <title>选择要监视的 CPU</title>
	    <screenshot>
	       <mediaobject lang="en">
		  <imageobject><imagedata fileref="figures/cpufreq-applet-preferences-smp.png" format="PNG"/>
		  </imageobject>
		  <textobject>
		     <phrase>CPU Frequency Scaling Monitor preferences dialog CPU selector</phrase>
		  </textobject>
	       </mediaobject>
	    </screenshot>
	 </figure>
	<note><para><guilabel>监视的 CPU</guilabel> 选项将只会在多处理器(SMP)系统中出现。</para></note>
	 
	     </listitem>
	   </varlistentry> 
	   <varlistentry> 
	      <term lang="en"><guilabel>Show in Mode</guilabel></term> 
	     <listitem> 
		<para>使用此下拉列表可选择您想要在小程序中以何种模式显示。可用的模式有：图形、文本和两者都有。</para> 
	    </listitem>
	 </varlistentry>
	 <varlistentry>
	    <term lang="en"><guilabel>Show CPU frequency as frequency</guilabel></term>
	    <listitem>
	       <para>选择此选项可显示 CPU 的当前频率。</para>
	    </listitem>
	 </varlistentry>
	 <varlistentry>
	    <term lang="en"><guilabel>Show frequency units</guilabel></term>
	    <listitem>
	       <para>选择此选项可设定在频率模式中是否显示频率单位。</para>
	    </listitem>
	 </varlistentry>
	  <varlistentry>
	     <term lang="en"><guilabel>Show CPU frequency as percentage</guilabel></term>
	     <listitem>
		<para>选择此选项可将 CPU 的当前频率显示为百分比。</para>
	     </listitem>
	  </varlistentry>

	  <varlistentry><term lang="en"><guilabel>Show menu</guilabel></term>
	   <listitem>
	    <para lang="en">
	     This option allows you to choose how much information is shown in
	     the frequency selector dropdown
	     (see <xref linkend="gnome-cpufreq-selector"/>). You can select to
	     display <guilabel>Frequencies</guilabel>,
	     <guilabel>Governors</guilabel> or both <guilabel>Frequencies and
	     Governors</guilabel>.
	 <figure id="cpufreq-applet-selector-both-2">
	    <title lang="en">The selector configured to Frequencies and Governors</title>
	    <screenshot>
	       <mediaobject lang="en">
		  <imageobject><imagedata fileref="figures/cpufreq-applet-selector-both.png" format="PNG"/>
		  </imageobject>
		  <textobject>
		     <phrase>CPU governors selector</phrase>
		  </textobject>
	       </mediaobject>
	    </screenshot>
	 </figure>

	    </para>
	  </listitem></varlistentry>
       </variablelist>
  </sect1>

</article>
