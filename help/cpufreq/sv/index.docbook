<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.12.0">
<!ENTITY applet "CPU Frequency Scaling Monitor">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 06, 2003
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="sv">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo> 
	<title>Handbok för Övervakare av processorfrekvensskalning</title> 
        <abstract role="description">
	  <para>Övervakare av processorfrekvensskalning visar aktuell klockhastighet för processorn och tillhandahåller ett gränssnitt för att ändra klockhastigheten om processorn har stöd för det.</para>
	</abstract>
	<copyright><year>2005</year> <holder>Davyd Madeley</holder></copyright>
        <copyright><year>2004</year> <holder>GNOME Foundation</holder></copyright>

<!-- translators: uncomment this:
  <copyright>
   <year>2003</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>
   -->

	 <publisher role="maintainer"> 
		<publishername>Dokumentationsprojekt för GNOME</publishername> 
	 </publisher> 


<!-- This file  contains link to license for the documentation (GNU FDL), and 
     other legal stuff such as "NO WARRANTY" statement. Please do not change 
     any of this. -->

	 <authorgroup> 
            <author><firstname>Carlos</firstname> <surname>Garcia Campos</surname> <affiliation> <orgname>GNOME-projektet</orgname> <address><email>carlosgc@gnome.org</email></address> </affiliation></author>
	    <author><firstname>Davyd</firstname> <surname>Madeley</surname> <affiliation> <orgname>GNOME-projektet</orgname> <address><email>davyd@madeley.id.au</email></address> </affiliation></author>
      <!-- This is appropriate place for other contributors: translators,
           maintainers,  etc. Commented out by default.
        <othercredit role="translator">
	      <firstname>Latin</firstname> 
	      <surname>Translator 1</surname> 
	      <affiliation> 
	      	<orgname>Latin Translation Team</orgname> 
	       	<address> <email>translator@gnome.org</email> </address> 
	      </affiliation>
	      <contrib>Latin translation</contrib>
        </othercredit>
-->
	 </authorgroup>
	 
	 <releaseinfo revision="2.26" role="review"/>

	 <revhistory>
	   <revision><revnumber>GNOME 2.12</revnumber> <date>Augusti 2005</date> <revdescription>
	      <para role="author">Davyd Madeley</para>
	     </revdescription></revision>
	   <revision><revnumber>GNOME 2.10</revnumber> <date>Mars 2005</date> <revdescription>
	      <para role="author">Davyd Madeley</para>
	     </revdescription></revision>
		<revision><revnumber>Handbok för Övervakare av processorfrekvensskalning V0.3</revnumber> <date>Oktober 2004</date> <revdescription> 
                    <para role="author">Carlos Garcia Campos</para>
                    <para role="publisher">Dokumentationsprojekt för GNOME</para>
		  </revdescription></revision> 
	 </revhistory> 

	 <releaseinfo>Denna handbok beskriver version 2.12.0 av Övervakare av processorfrekvensskalning.</releaseinfo> 

		<legalnotice> 
		<title>Återkoppling</title> 
		<para>För att rapportera ett fel eller komma med förslag angående miniprogrammet Övervakare av processorfrekvensskalning eller denna handbok, följ anvisningarna på <ulink url="ghelp:gnome-feedback" type="help">GNOME:s återkopplingssida</ulink>.</para>
	     </legalnotice>

  
    <othercredit class="translator">
      <personname>
        <firstname>Daniel Nylander</firstname>
      </personname>
      <email>po@danielnylander.se</email>
    </othercredit>
    <copyright>
      
        <year>2006</year>
      
      <holder>Daniel Nylander</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Anders Jonsson</firstname>
      </personname>
      <email>anders.jonsson@norsjovallen.se</email>
    </othercredit>
    <copyright>
      
        <year>2016</year>
      
      <holder>Anders Jonsson</holder>
    </copyright>
  </articleinfo> 

  <indexterm zone="index"><primary>Övervakare av processorfrekvensskalning</primary></indexterm>
<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<sect1 id="cpufreq-applet-introduction"> 
	 <title>Introduktion</title> 
	 <figure id="cpufreq-applet-figure"> 
	    <title>Övervakare av processorfrekvensskalning</title> 
		<screenshot> 
		  <mediaobject> 
		     <imageobject>
			<imagedata fileref="figures/cpufreq-applet.png" format="PNG"/> 
		     </imageobject>
		  </mediaobject>
	       </screenshot>
	 </figure> 
    <!-- ==== End of Figure ======================================= -->
    <para><application>Övervakare av processorfrekvensskalning</application> tillhandahåller ett smidigt sätt att övervaka processorfrekvensskalningen för varje processor.</para> 
	 <para>För att lägga till <application>Övervakare av processorfrekvensskalning</application> till en panel, högerklicka på panelen för att öppna panelens popupmeny, välj sedan <menuchoice> <guimenu>Lägg till i panel</guimenu><guimenuitem>Övervakare av processorfrekvensskalning</guimenuitem> </menuchoice>.</para> 
	
    <note><para>Tyvärr kan endast processorfrekvensskalning övervakas på Linux-maskiner som har stöd för det i kärnan. Den har dock stöd för flera generationer av frekvensskalningsgränssnitt i kärnan.</para></note>
   
   <para>När det inte finns stöd för processorfrekvensskalning i systemet, visar Övervakare av processorfrekvensskalning endast den aktuella processorfrekvensen.</para>
   <para>När processorfrekvensskalning stöds i systemet, visar Övervakare av processorfrekvensskalning processorikonen med en förloppsindikator. Tillståndet för förloppsindikatornn representerar den aktuella processorfrekvensen med maximal frekvens inräknad.</para>
   <para>Som standard visar Övervakare av processorfrekvensskalning den aktuella processorfrekvensen som ett värde i hertz (standardenheten för frekvens), men kan även konfigureras att visa en procentandel. För mer information om hur man konfigurerar Övervakare av processorfrekvensskalning, se <xref linkend="cpufreq-applet-prefs"/>.</para>
   <para>Varje instans av Övervakare av processorfrekvensskalning kan övervaka en processor. Du måste starta en instans av Övervakare av processorfrekvensskalning för varje processor som du vill övervaka om du använder ett system med flera processorer (SMP).</para>

   <sect2 id="gnome-cpufreq-icons">
     <title>Förstå vad ikonerna innebär</title>
     <para>Övervakare av processorfrekvensskalning har en serie ikoner som grafiskt visar den aktuella skalningsnivån för processorn du övervakar. De är:</para>
      <informaltable frame="all">
        <tgroup cols="2" colsep="1" rowsep="1">
          <colspec colname="COLSPEC0" colwidth="50*"/>
          <colspec colname="COLSPEC1" colwidth="50*"/>
          <thead>
            <row valign="top">
              <entry colname="COLSPEC0">
                <para>Ikon</para></entry>
              <entry colname="COLSPEC1">
                <para>Beskrivning</para></entry>
            </row>
          </thead>
          <tbody>
            <row valign="top">
              <entry><para><inlinemediaobject><imageobject> <imagedata fileref="figures/cpufreq-100.png" format="PNG"/> </imageobject><textobject> <phrase>100% styrka</phrase> </textobject></inlinemediaobject></para></entry>
	      <entry><para>Processorn kör på, eller nära, 100% styrka</para></entry>
	    </row>
            <row valign="top">
              <entry><para><inlinemediaobject><imageobject> <imagedata fileref="figures/cpufreq-75.png" format="PNG"/> </imageobject><textobject> <phrase>75% styrka</phrase> </textobject></inlinemediaobject></para></entry>
	      <entry><para>Processorn kör på, eller nära, 75% styrka</para>
	      </entry>
	    </row>
            <row valign="top">
              <entry><para><inlinemediaobject><imageobject> <imagedata fileref="figures/cpufreq-50.png" format="PNG"/> </imageobject><textobject> <phrase>50% styrka</phrase> </textobject></inlinemediaobject></para></entry>
	      <entry><para>Processorn kör på, eller nära, 50% styrka</para></entry>
	    </row>
            <row valign="top">
              <entry><para><inlinemediaobject><imageobject> <imagedata fileref="figures/cpufreq-25.png" format="PNG"/> </imageobject><textobject> <phrase>25% styrka</phrase> </textobject></inlinemediaobject></para></entry>
	      <entry><para>Processorn kör på, eller nära, 25% styrka</para></entry>
	    </row>
	  </tbody>
	</tgroup>
      </informaltable>
   </sect2>
   
   <sect2 id="gnome-cpufreq-selector">
      <title>Väljare för frekvens och regulator</title>
      <warning><para>Frekvensväljarens funktionalitet kanske inte finns tillgänglig på ditt GNOME-skrivbord som standard. Konsultera din systemadministratör, tillverkarens dokumentation eller dokumentationen som kommer med denna programvara.</para></warning>
      <para>För att ställa in processorfrekvensen, (vänster)klicka på övervakaren och en meny kommer att visas. Denna meny visar dig en lista på tillgängliga frekvenser och/eller frekvensregulatorer för din dator.</para>
      <tip><para>Du kan välja huruvida du vill visa tillgängliga frekvenser eller regulatorer i <guilabel>Inställningar</guilabel>. Se <xref linkend="cpufreq-applet-prefs"/> för mer information.</para></tip>

      <figure id="cpufreq-applet-selector-img">
	 <title>Frekvensväljaren visar bara frekvenser</title>
	 <screenshot>
	    <mediaobject>
	       <imageobject>
		  <imagedata fileref="figures/cpufreq-applet-selector.png" format="PNG"/>
	       </imageobject>
	    </mediaobject>
	</screenshot>
      </figure>

      <figure id="cpufreq-applet-selector-both-img">
	 <title>Frekvensväljaren visar både frekvenser och regulatorer</title>
	 <screenshot>
	    <mediaobject>
	       <imageobject>
		  <imagedata fileref="figures/cpufreq-applet-selector-both.png" format="PNG"/>
	       </imageobject>
	    </mediaobject>
	</screenshot>
      </figure>
   </sect2>
</sect1>

<!-- ============= Customization ============================= -->
<!-- Use this section to describe how to customize the applet. -->
<sect1 id="cpufreq-applet-prefs"> 
	 <title>Inställningar</title> 
	 <para>För att konfigurera <application>Övervakare av processorfrekvensskalning</application>, högerklicka på miniprogrammet, välj sedan <guimenuitem>Inställningar</guimenuitem> från popupmenyn.</para> 
	 <para>Dialogrutan <guimenuitem>Inställningar</guimenuitem> innehåller följande komponenter:</para> 
	 <figure id="cpufreq-applet-preferences">
	    <title>Inställningsdialogen i Övervakare av processorfrekvensskalning</title>
	    <screenshot>
	       <mediaobject><imageobject><imagedata fileref="figures/cpufreq-applet-preferences.png" format="PNG"/> </imageobject> <textobject> <phrase>Inställningsdialogen i Övervakare av processorfrekvensskalning</phrase> </textobject></mediaobject>
	    </screenshot>
	 </figure>

	 <variablelist> 
	   <varlistentry> 
	      <term><guilabel>Övervakad processor (endast för system med flera processorer)</guilabel></term> 
	     <listitem> 
		<para>Använd denna rullgardinsmeny för att välja processorn som du vill övervaka.</para>
	 <figure id="cpufreq-applet-preferences-smp">
	    <title>Välja processor att övervaka</title>
	    <screenshot>
	       <mediaobject><imageobject><imagedata fileref="figures/cpufreq-applet-preferences-smp.png" format="PNG"/> </imageobject> <textobject> <phrase>Processorväljare i inställningsdialogen för Övervakare av processorfrekvensskalning</phrase> </textobject></mediaobject>
	    </screenshot>
	 </figure>
	<note><para>Alternativet <guilabel>Övervakad processor</guilabel> visas endast på system med flera processorer (SMP).</para></note>
	 
	     </listitem>
	   </varlistentry> 
	   <varlistentry> 
	      <term><guilabel>Visa i läge</guilabel></term> 
	     <listitem> 
		<para>Använd denna rullgardinsmeny för att välja läget i vilket du vill visa miniprogrammet. Tillgängliga lägen är: Grafik, text och båda (grafik och text)</para> 
	    </listitem>
	 </varlistentry>
	 <varlistentry>
	    <term><guilabel>Visa processorfrekvens som frekvens</guilabel></term>
	    <listitem>
	       <para>Välj detta alternativ för att visa aktuell processorfrekvens.</para>
	    </listitem>
	 </varlistentry>
	 <varlistentry>
	    <term><guilabel>Visa frekvensenheter</guilabel></term>
	    <listitem>
	       <para>Välj detta alternativ för att visa (eller inte visa) frekvensenheterna i frekvensläget.</para>
	    </listitem>
	 </varlistentry>
	  <varlistentry>
	     <term><guilabel>Visa processorfrekvens som procentandel</guilabel></term>
	     <listitem>
		<para>Välj detta alternativ för att visa aktuell processorfrekvens som en procentandel.</para>
	     </listitem>
	  </varlistentry>

	  <varlistentry><term><guilabel>Visa meny</guilabel></term>
	   <listitem>
	    <para>Detta alternativ låter dig välja hur mycket information som ska visas i frekvensväljarens rullgardinsmeny (se <xref linkend="gnome-cpufreq-selector"/>). Du kan välja att visa <guilabel>Frekvenser</guilabel>, <guilabel>Regulatorer</guilabel> eller både <guilabel>Frekvenser och regulatorer</guilabel>. <figure id="cpufreq-applet-selector-both-2">
	    <title>Väljaren konfigurerad till Frekvenser och Regulatorer</title>
	    <screenshot>
	       <mediaobject><imageobject><imagedata fileref="figures/cpufreq-applet-selector-both.png" format="PNG"/> </imageobject> <textobject> <phrase>Processorregulatorväljare</phrase> </textobject></mediaobject>
	    </screenshot>
	 </figure></para>
	  </listitem></varlistentry>
       </variablelist>
  </sect1>

</article>
