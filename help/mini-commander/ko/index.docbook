<?xml version="1.0" encoding="utf-8"?>
<!-- -*- indent-tabs-mode: nil -*- -->
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appletversion "2.12">
<!ENTITY applet "Command Line">
<!ENTITY mdash "&#8212;">
]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp 
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
  
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="ko">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
<articleinfo> 
  <title lang="en">Command Line Manual</title> 
  <abstract role="description">
    <para lang="en">Command Line allows commands to be run from a panel,
      and includes quick access to a file browser and command history.</para>
  </abstract>
  <copyright lang="en">
    <year>2005</year>
    <holder>Davyd Madeley</holder>
  </copyright>
  <copyright lang="en">
    <year>2004</year>
    <holder>Angela Boyle</holder>
  </copyright>
  <copyright lang="en"> 
    <year>2002</year>
    <year>2003</year> 
    <year>2004</year> 
    <holder>Sun Microsystems</holder> 
  </copyright>    
  <copyright lang="en"> 
    <year>1998</year> 
    <holder>Oliver Maruhn</holder> 
  </copyright>
  <copyright lang="en"> 
    <year>1998</year> 
    <holder>Dan Mueth</holder> 
  </copyright>
  <!-- translators: uncomment this:
       <copyright>
       <year>2003</year>
       <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
       </copyright>
  -->
  <publisher role="maintainer"> 
    <publishername>그놈 문서 프로젝트</publishername> 
  </publisher>

  

  <authorgroup>
    <author lang="en">
      <firstname>Sun </firstname>
      <surname>GNOME Documentation Team</surname>
      <affiliation>
        <orgname>Sun Microsystems</orgname>
      </affiliation>
    </author>
    <author lang="en">
      <firstname>Oliver</firstname>
      <surname>Maruhn</surname>
      <affiliation>
        <orgname>GNOME Documentation Project</orgname>
	<address><email>oliver@maruhn.com</email></address>
      </affiliation>
    </author>
    <author lang="en">
      <firstname>Dan</firstname>
      <surname>Mueth</surname>
      <affiliation>
        <orgname>GNOME Documentation Project</orgname>
	<address><email>d-mueth@uchicago.edu</email></address>
      </affiliation>
    </author>
    <author lang="en">
      <firstname>Angela</firstname>
      <surname>Boyle</surname>
      <affiliation>
        <orgname>GNOME Documentation Project</orgname>
      </affiliation>
    </author>
    <author lang="en">
      <firstname>Davyd</firstname><surname>Madeley</surname>
      <affiliation>
        <orgname>GNOME Project</orgname>
	<address><email>davyd@madeley.id.au</email></address>
      </affiliation>
    </author>
    <!-- This is appropriate place for other contributors: translators,
         maintainers,  etc. Commented out by default.
         <othercredit role="translator">
         <firstname>Latin</firstname> 
         <surname>Translator 1</surname> 
         <affiliation> 
         <orgname>Latin Translation Team</orgname> 
         <address> <email>translator@gnome.org</email> </address> 
         </affiliation>
         <contrib>Latin translation</contrib>
         </othercredit>
    -->
  </authorgroup> 
  <revhistory>
    <revision lang="en">
      <revnumber>Version 2.12</revnumber>
      <date>September 2005</date>
      <revdescription>
        <para role="author" lang="en">Davyd Madeley</para>
      </revdescription>
    </revision>
    <revision lang="en">
      <revnumber>Version 2.10</revnumber>
      <date>March 2005</date>
      <revdescription>
        <para role="author" lang="en">Davyd Madeley</para>
      </revdescription>
    </revision>
    <revision lang="en">
      <revnumber>Version 2.8</revnumber>
      <date>September 2004</date>
      <revdescription>
        <para role="author" lang="en">Angela Boyle</para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
    <revision lang="en"> 
      <revnumber>Command Line Applet Manual V2.7</revnumber> 
      <date>August 2004</date> 
      <revdescription> 
        <para role="author" lang="en">Sun Microsystems</para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription> 
    </revision>
    <revision lang="en"> 
      <revnumber>Command Line Applet Manual V2.6</revnumber> 
      <date>September 2003</date> 
      <revdescription> 
        <para role="author" lang="en">Sun Microsystems</para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription> 
    </revision>
    <revision lang="en"> 
      <revnumber>Command Line Applet Manual V2.5</revnumber> 
      <date>August 2003</date> 
      <revdescription> 
        <para role="author" lang="en">Sun Microsystems</para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription> 
    </revision>
    <revision lang="en"> 
      <revnumber>Command Line Applet Manual V2.4</revnumber> 
          <date>April 2003</date> 
          <revdescription> 
             <para role="author" lang="en">
                Sun Microsystems
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision>
        
			<revision lang="en"> 
          <revnumber>Command Line Applet Manual V2.3</revnumber> 
          <date>January 2003</date> 
          <revdescription> 
             <para role="author" lang="en">
                Sun Microsystems
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision> 

        <revision lang="en"> 
          <revnumber>Command Line Applet Manual V2.2</revnumber> 
          <date>August 2002</date> 
          <revdescription> 
             <para role="author" lang="en">
                Sun Microsystems
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision> 
        
         
        <revision lang="en"> 
          <revnumber>Command Line Applet Manual V2.1</revnumber> 
          <date>July 2002</date> 
          <revdescription> 
             <para role="author" lang="en">
                Sun Microsystems
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision> 
        <revision lang="en"> 
          <revnumber>Command Line Applet Manual V2.0</revnumber> 
          <date>March 2002</date> 
          <revdescription> 
             <para role="author" lang="en">
                Sun Microsystems
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision> 
        <revision lang="en"> 
          <revnumber>Mini-Commander Applet Manual</revnumber> 
          <date>1998</date> 
          <revdescription> 
             <para role="author" lang="en">
                Oliver Maruhn <email>oliver@maruhn.com</email>
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
             <para role="author" lang="en">
                Dan Mueth <email>d-mueth@uchicago.edu</email>
             </para>
             <para role="publisher" lang="en">
                GNOME Documentation Project
             </para>
          </revdescription> 
        </revision> 
     </revhistory> 
     <releaseinfo lang="en"> This manual describes version 2.12 of Command Line.
        </releaseinfo> 
     <legalnotice> 
        <title>피드백</title> 
        <para lang="en">
          To report a bug or make a suggestion regarding the Command Line applet or this manual, follow the directions in the <ulink url="ghelp:gnome-feedback" type="help">GNOME Feedback Page</ulink>. 
        </para>
     </legalnotice> 
  </articleinfo> 
  <indexterm zone="index" lang="en"> 
     <primary>Command Line</primary> 
  </indexterm> 
  <indexterm zone="index" lang="en"> 
     <primary>command line applet</primary> 
  </indexterm>

<!-- ============= Introduction =====-->
  <sect1 id="command-line-introduction"> 
     <title>소개</title> 
     
     <!-- ==== Figure ==================-->
     <figure id="applet-fig"> 
        <title lang="en">Command Line</title> 
        <screenshot> 
          <mediaobject lang="en"> 
             <imageobject><imagedata fileref="figures/command-line.png" format="PNG"/> 
             </imageobject> 
             <textobject> 
                <phrase>Shows Command Line.</phrase> 
             </textobject> 
          </mediaobject> 
        </screenshot> 
     </figure> 
     <para lang="en">
        The <application>Command Line</application> provides a command line that you can use within any panel on the desktop. </para>
     <para lang="en">
        <application>Command Line</application> contains the following interactive elements: </para>
        
        <variablelist>
          <varlistentry>
             <term lang="en">
                <guilabel>Entry</guilabel> field</term>
             <listitem>
                <para lang="en">
                  You can enter commands, macros, and programs in the <guilabel>entry field</guilabel>.
                </para>
             </listitem>
          </varlistentry>
          <varlistentry>
             <term lang="en">
                <guilabel>Browser</guilabel> button — the bullet</term>
             <listitem>
                <para lang="en">
                  You can use the <guibutton>Browser</guibutton> button to look through your file system for programs. A solid-circle icon identifies the <guibutton>Browser</guibutton> button.
                </para>
             </listitem>
          </varlistentry>
          <varlistentry>
             <term lang="en">
                <guibutton>History</guibutton> button — the down arrow</term>
             <listitem>
                <para lang="en">
                  You can use the <guibutton>History</guibutton> button to recall previous commands. A down-arrow icon identifies the <guibutton>History</guibutton> button.
                </para>
             </listitem>
          </varlistentry>
        </variablelist> 
        <sect2 id="features">
	<title lang="en">Features</title>        
        <variablelist>
          <varlistentry>
             <term lang="en">History list of previously executed commands</term>
             <listitem>
                <para lang="en">
                  You can execute commands directly from the history list display. You can also cycle the history list contents through the <guilabel>entry field</guilabel> to display and then execute a command. A vertical scrollbar appears when more than 20 commands are in the history list. See <xref linkend="command-line-histlist"/> for more information.
                </para>
             </listitem>
          </varlistentry>
          <varlistentry>
             <term lang="en">History-based auto completion function</term>
             <listitem>
                <para lang="en">
                  <application>Command Line</application> uses the history-based auto completion function to complete commands in the <guilabel>entry field</guilabel>. This function compares the contents of the <guilabel>entry field</guilabel> with the history list. If <application>Command Line</application> finds a match then the applet completes the command. See the note in <xref linkend="command-line-histlist"/> for more information.
                </para>
             </listitem>
          </varlistentry>
          <varlistentry>
             <term lang="en">User-requestable command completion function</term>
             <listitem>
                <para lang="en">
                  This command completion function compares character strings in the <guilabel>entry field</guilabel> with commands in the user path. The command completion function is useful if you do not know the exact character sequence or spelling of a command. After you type the first few letters of the command name, press the <keycap>Tab</keycap> key. If <application>Command Line</application> recognizes the character sequence as unique, then the applet completes the command name. See <xref linkend="command-line-run"/> for more information.
                </para>
             </listitem>
          </varlistentry>
          <varlistentry>
             <term lang="en">Macro facility</term>
             <listitem>
                <para lang="en">
                  The macro facility provides up to 99 shortcut commands. In addition to predefined commands you can create your own commands for use with <application>Command Line</application>. See <xref linkend="command-line-macros"/> for more information.
                </para>
             </listitem>
          </varlistentry>
        </variablelist>
	</sect2>

      <sect2 id="command-line-introduction-add">       
        <title lang="en">To Add Command Line to a Panel</title>
        <para lang="en">To add <application>Command Line</application> to a panel,
        right-click on the panel, then choose <guimenuitem>Add to
        Panel</guimenuitem>.  Select <application>Command Line</application>
        in the <application>Add to the panel</application> dialog, then
        click <guibutton>OK</guibutton>.</para>
      </sect2>

  </sect1>
<!-- ================ Running Commands =======-->
  <sect1 id="command-line-usage"> 
     <title lang="en">Running Commands</title>
	<!-- ================ To Run Command =======-->
     <sect2 id="command-line-run">
        <title lang="en">To Run a Command From the Entry Field</title>
        <para lang="en">
          To run a command from the <guilabel>entry field</guilabel>, perform the following steps: 
        </para>
        <orderedlist> 
          <listitem>
             <para lang="en">
                Type the command in the <guilabel>entry field</guilabel>. 
             </para>
             <para lang="en">
                To instruct <application>Command Line </application> to try to complete the command name, press the <keycap>Tab</keycap> key.
             </para>
          </listitem> 
          <listitem>
             <para lang="en">
                Type the command-line options, if any, in the <guilabel>entry field</guilabel>. 
             </para>
          </listitem> 
          <listitem>
             <para lang="en">
                Press <keycap>Return</keycap>. 
             </para>
          </listitem> 
        </orderedlist>
	<para lang="en">
	 If the command you entered could not be run, the computer will emit a
	 single warning beep and the applet will indicate the command could not
	 be run by showing a question mark (?) before the command. Check that
	 you specified the command correctly. Clicking on the command line will
	 cause the question mark to automatically disappear.
	</para>
	<tip>
        <para lang="en">If the auto completion option is selected, <application>Command Line</application> searches for a match in the history list during steps 1 and 2. See <xref linkend="command-line-histauto"/> for more information.</para>
	</tip>
	</sect2>
<sect2 id="command-line-histauto">
		<title lang="en">History-based auto completion</title>
		<para lang="en">You can enable or disable this option under the <guimenu>Preferences</guimenu> menu in the <guilabel>General</guilabel> tab under <guilabel>Auto Completion</guilabel>. Select <guilabel>Enable history-based auto completion</guilabel> to instruct <application>Command Line</application> to compare the <guilabel>entry field</guilabel> with the history list, and complete commands when there is a match. </para>
	        <para lang="en">If the auto completion option is selected, <application>Command Line</application> searches for a match in the history list during steps 1 and 2 in <xref linkend="command-line-run"/>. If there is a match in the history list, <application>Command Line</application> completes the command. To accept the command, press <keycap>Return</keycap>. If you do not want to accept the command, type more characters in the <guilabel>entry field</guilabel>. 
        </para>
     </sect2>
	<!-- ================ To Run Program =======-->
     <sect2 id="command-line-filesys"> 
        <title lang="en">To Run a Program From Your File System</title> 
        <para lang="en">
          Click on the <guibutton>Browser</guibutton> button to display the file system, then double-click on the program you want to run. 
        </para>
        <para lang="en">
          You can use <application>Command Line</application> to run scripts, execute commands, or run other programs from your file system. 
        </para>
     </sect2> 
     <sect2 id="command-line-histlist"> 
        <title lang="en">To Run a Previous Command, Macro, or Program From the History List
          </title> 
        <para lang="en">
          You can run a previously executed command, macro, or program from the history list in one of the following ways: 
          <itemizedlist> 
             <listitem>
                <para lang="en">
                  Click on the <guibutton>History</guibutton> button to display the history list. Click on a history list item to run the command, macro, or program that you require.
                </para>
             </listitem> 
             <listitem>
                <para lang="en">
                  Click on the <guilabel>entry field</guilabel>, then use the keyboard up and down arrows to cycle through the history list. Press <keycap>Return</keycap> to run a specific command, macro, or program when the history list item you want is displayed in the <guilabel>entry field</guilabel>.
                </para>
             </listitem> 
          </itemizedlist>
        </para>

	<note>
		<title lang="en">History-based auto completion</title>
		<para lang="en">You can enable or disable this option under the <guimenu>Preferences</guimenu> menu in the <guilabel>General</guilabel> tab under <guilabel>Auto Completion</guilabel>. Select <guilabel>Enable history-based auto completion</guilabel> to instruct <application>Command Line</application> to compare the <guilabel>entry field</guilabel> with the history list, and complete commands when there is a match. </para>
	</note>
     </sect2> 
</sect1>
<!-- ================ Using Macros =======-->
<sect1 id="command-line-macros">
	<title lang="en">Using Shortcuts or Macros</title>
                <para lang="en">
                  The macro facility provides up to 99 shortcut commands. In addition to predefined commands you can create your own commands for use with <application>Command Line</application>.
                </para>
     <sect2 id="command-line-runmacro"> 
        <title lang="en">To Run a Macro From the Entry Field</title> 
        <para lang="en">
          To run a macro that is defined in the <application>Command Line</application> <xref linkend="command-line-prefs-2"/> list, type the macro in the <guilabel> entry field</guilabel>, then press <keycap>Return</keycap>. 
        </para>
        <para lang="en">
          The <xref linkend="command-line-histauto"/> function also works for macros. 
        </para>
        <para lang="en">
          The following list shows some examples of <application>Command Line</application> macros: 
        </para>
        <informaltable frame="none"> 
          <tgroup cols="2" colsep="0" rowsep="0"><colspec colwidth="29.29*"/><colspec colwidth="70.71*"/> 
             <tbody> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>http://</userinput> or
		  <userinput>www.</userinput></para></entry> 
                  <entry><para lang="en">
                   Anything starting with <userinput>http://</userinput> or
		   <userinput>www.</userinput> will be
		   treated as a URL and loaded in your web browser.
		  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>ftp://</userinput> or
		  <userinput>ftp.</userinput></para></entry> 
                  <entry><para lang="en">
                   Anything starting <userinput>ftp://</userinput> or
		   <userinput>ftp.</userinput> will be
		   treated as a URL and loaded in your default FTP software.
                  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>dictionary:</userinput></para></entry> 
                  <entry><para lang="en">
		   Use <userinput>dictionary:</userinput> to look up a word
		   using <application>GNOME Dictionary</application>.
                  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>google:</userinput></para></entry> 
                  <entry><para lang="en">
		   Use <userinput>google:</userinput> to search for something
		   using the Google search engine.
                  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>bing:</userinput></para></entry> 
                  <entry><para lang="en">
		   Use <userinput>bing:</userinput> to search for something using
		   the Bing search engine.
                  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>ddg:</userinput></para></entry> 
                  <entry><para lang="en">
		   Use the <userinput>ddg:</userinput> macro to search for
		   things using the DuckDuckGo search engine.
                  </para></entry> 
                </row> 
                <row valign="top"> 
                  <entry><para lang="en"><userinput>w:</userinput></para></entry> 
                  <entry><para lang="en">
		   Use <userinput>w:</userinput> to search for articles in
		   Wikipedia.
                  </para></entry> 
                </row> 
             </tbody> 
          </tgroup> 
        </informaltable>
     </sect2> 
    <sect2 id="command-line-prefs-2"> 
        <title lang="en">Customizing Your Macros</title> 
     <figure id="applet-macro-fig"> 
        <title lang="en">Configuring Macros</title> 
        <screenshot> 
          <mediaobject lang="en"> 
             <imageobject><imagedata fileref="figures/prefs-macros.png" format="PNG"/> 
             </imageobject> 
             <textobject> 
                <phrase>Configuring Macros</phrase> 
             </textobject> 
          </mediaobject> 
        </screenshot> 
     </figure> 
        <para lang="en">
          This tabbed section contains a list of macros that you can use with <application>Command Line</application>. The list contains the following information: 
			</para>
          <itemizedlist> 
             <listitem>
                <para lang="en">
                  A list of UNIX regular expressions in the <guilabel>Pattern</guilabel> column. 
                </para>
                <para lang="en">
                  A regular expression is a pattern of characters or numbers that you use to match strings. See the <command>regex</command> man page for further information on how to construct a regular expression. 
                </para>
             </listitem> 
             <listitem>
                <para lang="en">
                  A list of commands in the <guilabel>Command</guilabel> column.
                </para>
                <para lang="en">
                  A command executes if the corresponding pattern successfully matches the text that you type into the <guilabel>entry field</guilabel>. The macros are commands that can take parameters parsed by the regular expression.
                </para>
             </listitem> 
          </itemizedlist> 
		<para lang="en">To add a new macro, click on the <guibutton>Add Macro</guibutton> button. To delete a macro, select the macro, then click on the <guibutton>Delete Macro</guibutton> button. 
     </para>
     </sect2> 
</sect1>

<!-- ============= Appearance ============================= -->
  <sect1 id="command-line-apperance"> 
     <title lang="en">Customizing the Appearance</title> 
     <para lang="en">
     <figure id="applet-prefs-fig"> 
        <title lang="en">Preferences</title> 
        <screenshot> 
          <mediaobject lang="en"> 
             <imageobject><imagedata fileref="figures/prefs.png" format="PNG"/> 
             </imageobject> 
             <textobject> 
                <phrase>Preferences Dialog</phrase> 
             </textobject> 
          </mediaobject> 
        </screenshot> 
     </figure> 
        To configure the appearance of <application>Command Line</application>, right-click on an area of the applet outside the interactive elements, then choose <guimenuitem>Preferences</guimenuitem> and select the <guilabel>General</guilabel> tab.
     </para>
        <variablelist>
		<varlistentry>
		<term lang="en">To move Command Line: </term>
		<listitem>
		<para lang="en">Select <guilabel>Show handle</guilabel> to attach a handle to the edge of the applet. You can use the handle to drag the <application>Command Line</application> display to any location on your desktop. </para>
		</listitem>
		</varlistentry>
		<varlistentry>
		<term lang="en">To add a border:</term>
		<listitem>
		<para lang="en">Select this option to show a frame around the <application>Command Line</application>.</para>
		</listitem>
		</varlistentry>
		<varlistentry>
		<term lang="en">To change the width:</term>
		<listitem>
		<para lang="en">Use the <guilabel>Width . . . pixels</guilabel> spin box to specify the applet width.</para>
		</listitem>
		</varlistentry>
		<varlistentry>
		<term lang="en">To adjust the color theme:</term>
		<listitem>
			<itemizedlist>
				<listitem><para lang="en">Select <guilabel>Use default theme colors</guilabel> to use the colors from the default GNOME theme.</para></listitem>
				<listitem><para lang="en">Click on the <guilabel>Command line foreground</guilabel> button to select a color for the command line text. This button is unavailable if the <guilabel>Use default theme colors</guilabel> check box is selected.</para></listitem>
				<listitem><para lang="en">Click on the <guilabel>Command line background</guilabel> button to select a color for the <guilabel>entry field</guilabel> background. This button is unavailable if the <guilabel>Use default theme colors</guilabel> check box is selected.</para></listitem>
			</itemizedlist>
		</listitem>
		</varlistentry>	
	</variablelist>

  </sect1> 
</article>
